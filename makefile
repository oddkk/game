VERSION = 0.1

CC = cc

FT_CFLAGS = $(shell freetype-config --cflags)
FT_LDFLAGS = $(shell freetype-config --libs)

INCS = -I. -I/usr/include
LIBS = -L/usr/lib -lm -lc -lGL -lGLEW -lglfw -lGLU -lX11

CPPFLAGS = -DVERSION=\"${VERSION}\" -D_DEFAULT_SOURCE -D_POSIX_C_SOURCE=200809L
CFLAGS = -ggdb -std=c11 -Wall -Wextra -O0 $(INCS) $(CPPFLAGS) $(FT_CFLAGS)
LDFLAGS = ${LIBS} $(FT_LDFLAGS)

SRC_DIR = src/
BUILD_DIR = build/

SRC = $(shell find src/ -name "*.c")

OBJ = $(SRC:$(SRC_DIR)%.c=$(BUILD_DIR)%.o)

all: game

$(BUILD_DIR):
	mkdir $(BUILD_DIR)

$(BUILD_DIR)%.o: $(SRC_DIR)%.c makefile
	@test -d $(BUILD_DIR) || mkdir $(BUILD_DIR)
	@echo "CC $<"
	@$(CC) -c -o $@ $< $(CFLAGS)

game: $(OBJ) makefile
	@echo "LD $@"
	@$(CC) -o $@ $(OBJ) $(LDFLAGS)

clean:
	@rm -f game
	@rm -rf $(BUILD_DIR)

rebuild:
	@$(MAKE) --no-print-directory clean
	@$(MAKE) --no-print-directory all

.PHONY: all clean rebuild

include sourcedeps
