#version 330

layout(location=0) in vec2 in_vec;
layout(location=1) in vec2 in_uv;

uniform ivec2 in_screen_size;
uniform ivec2 in_atlas_size;
uniform ivec2 in_glyph_pos;
uniform ivec2 in_glyph_size;
uniform vec2 in_glyph_bearing;
uniform vec2 in_cursor;
uniform mat3 in_matrix;

out vec2 frag_uv;

void main() {
	vec2 pos
		= in_vec * in_glyph_size
		- in_screen_size / 2
		+ in_cursor
		+ vec2(in_glyph_bearing.x,
		       in_glyph_bearing.y - in_glyph_size.y);
	pos = (vec3(pos, 1.0) * in_matrix).xy;
	vec2 screen_space = 2 * (pos / in_screen_size);
	gl_Position = vec4(screen_space, 0.0, 1.0);
	frag_uv = (in_glyph_pos + in_glyph_size * in_uv) / in_atlas_size;
}
