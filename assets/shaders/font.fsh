#version 330

uniform sampler2D in_atlas;
uniform vec4 in_color;

in vec2 frag_uv;

layout(location=0) out vec4 out_color;

void main() {
	float v = texture(in_atlas, frag_uv).x;
	out_color = in_color * vec4(1,1,1,v);
}
