#version 330

layout(location=0) in vec3 in_vec;
layout(location=1) in vec2 in_uv;
layout(location=2) in vec3 in_normal;

uniform mat4x4 in_projection;
uniform mat4x4 in_modelview;
uniform mat4x4 in_modelview_normal;

out vec3 frag_pos;
out vec3 frag_normal;
out vec2 frag_uv;

void main() {
	frag_pos = (vec4(in_vec, 1.0) * in_modelview).xyz;
	frag_normal = (vec4(in_normal, 1.0) * in_modelview_normal).xyz;
	gl_Position = vec4(frag_pos, 1.0) * in_projection;
	frag_uv = in_uv;
}
