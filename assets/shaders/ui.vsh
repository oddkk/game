#version 330

layout(location=0) in vec2 in_vec;
layout(location=1) in vec2 in_uv;

out vec2 frag_uv;

void main() {
	gl_Position = vec4(in_vec, 0.0, 1.0);
	frag_uv = in_uv;
}
