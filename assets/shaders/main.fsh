#version 330

in vec3 frag_pos;
in vec3 frag_normal;
in vec2 frag_uv;

layout(location=0) out vec4 out_color;

uniform vec3 in_ambient;
uniform vec3 in_diffuse;
uniform sampler2D in_diffuse_tex;
uniform vec3 in_specular;
uniform sampler2D in_specular_tex;
uniform float in_specular_exp;
uniform sampler2D in_noise;
uniform float in_dissolve;
uniform float in_transparancy;
uniform vec3 in_camera_pos;

const float in_gamma = 0.8;

struct light_point {
	vec3 position;

	vec3 diffuse;
	vec3 specular;
};

struct light_box {
	vec3 point1;
	vec3 point2;
	vec3 position;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

#define LIGHT_POINT_MAX 20
#define LIGHT_BOX_MAX 10 

layout(std140) uniform ShaderDataBlock {
	light_point in_light_point[LIGHT_POINT_MAX];
	light_box in_light_box[LIGHT_BOX_MAX];
};

vec3 calc_blinn_phong(vec3 pos, vec3 diff, vec3 spec) {
	vec3 normal = normalize(frag_normal);
	vec3 light_dir = normalize(pos - frag_pos);

	float lambertian = max(dot(light_dir, normal), 0.0);
	float specular = 0.0;

	if (lambertian > 0.0) {
		vec3 view_dir = normalize(in_camera_pos - frag_pos);
		vec3 half_dir = normalize(light_dir + view_dir);
		float spec_angle = max(dot(half_dir, normal), 0.0);
		specular = pow(spec_angle, in_specular_exp);
	}

	return lambertian * in_diffuse * diff
		 + specular * in_specular * spec;
}

vec3 calc_light_point(light_point light) {
	return calc_blinn_phong(light.position, light.diffuse, light.specular);
}

float inside_box(vec3 v, vec3 bottomLeft, vec3 topRight) {
    vec3 s = step(bottomLeft, v) - step(topRight, v);
    return s.x * s.y;
}

vec3 calc_light_box(light_box light) {
	return (light.ambient * in_diffuse
	      + calc_blinn_phong(light.position, light.diffuse, light.specular)
			) * inside_box(frag_pos, light.point1, light.point2);
}

void main() {
	if (in_dissolve == 1 || texture(in_noise, frag_uv).x < in_dissolve) {
		discard;
	}

	vec3 color_linear
		= in_ambient;

	for (int i = 0; i < LIGHT_POINT_MAX; ++i) {
		color_linear += calc_light_point(in_light_point[i]);
	}

	for (int i = 0; i < LIGHT_BOX_MAX; ++i) {
		color_linear += calc_light_box(in_light_box[i]);
	}

	/*
	light_point bonfire;
	bonfire.position = vec3(-1.5, 0.5, -1.5);
	bonfire.diffuse = vec3(0.6, 0.3, 0.0);
	bonfire.specular = vec3(0.6, 0.3, 0.0);

	color_linear += calc_light_point(bonfire);

	light_box moonlight;
	moonlight.point1 = vec3(-100, -100, -100);
	moonlight.point2 = vec3(100, 100, 100);
	moonlight.position = vec3(75, 100, 100);
	moonlight.ambient = vec3(0.05, 0.05, 0.1);
	moonlight.diffuse = vec3(0.2, 0.2, 0.3);
	moonlight.specular = vec3(0.3, 0.3, 0.45);

	color_linear += calc_light_box(moonlight);
	*/

	vec3 color_gammacorrected = pow(color_linear,
	                                vec3(1.0/in_gamma));
	
	out_color = vec4(color_gammacorrected, in_transparancy);
	//gl_FragColor = vec4(color_gammacorrected, 1);
}
