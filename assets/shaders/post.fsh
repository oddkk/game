#version 330

uniform sampler2D in_framebuffer;
uniform vec2 in_screensize;

in vec2 frag_uv;

layout(location=0) out vec4 out_color;

void main() {
	float factor = 1;
	//gl_FragColor = vec4(texture(in_framebuffer, frag_uv).xyz, 1.0)
		//* vec4(factor, factor, factor, 1);
	out_color = vec4(texture(in_framebuffer, frag_uv).xyz, 1.0)
		* vec4(factor, factor, factor, 1);
}
