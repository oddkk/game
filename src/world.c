#include "world.h"
#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>
#include <string.h>
#include <assert.h>
#include "assets.h"
#include "object_render.h"

bool
world_area_load(struct world *world, struct world_area *area) {
	const char *token_separator = " \n\r";
	size_t line_len = 0;
	char *line = NULL;
	FILE *fp = NULL;

	if (area->file == NULL) {
		return false;
	}

	fp = fopen(area->file, "r");

	if (!fp) {
		return false;
	}

	if (area->models != NULL) {
		free(area->models);
		area->models = NULL;
		area->num_models = 0;
	}

	if (area->geometry != NULL) {
		free(area->geometry);
		area->geometry = NULL;
		area->num_geometry = 0;
	}

	while (getline(&line, &line_len, fp) != -1) {
		char *line_state;
		char *token = strtok_r(line, token_separator, &line_state);

		if (!token)
			continue;

		/*  */ if (strcmp(token, "Am") == 0) {
			// Asset model
			// Am <name> <file>
			area->models = realloc(area->models, sizeof(struct world_asset_model) * (++area->num_models));
			struct world_asset_model *model = &area->models[area->num_models - 1];
			model->name = strdup(strtok_r(NULL, token_separator, &line_state));
			model->model = model_store_get(world->model_store, strtok_r(NULL, token_separator, &line_state));
		} else if (strcmp(token, "g") == 0) {
			// Geometry
			// g <model> <position> <scale> <rotation>
			area->geometry = realloc(area->geometry, sizeof(struct geometry) * (++area->num_geometry));
			struct geometry *geometry = &area->geometry[area->num_geometry - 1];
			char *model_name = strtok_r(NULL, token_separator, &line_state);

			geometry->model = NULL;

			for (size_t i = 0; i < area->num_models; ++i) {
				if (strcmp(area->models[i].name, model_name) == 0) {
					geometry->model = area->models[i].model;
					break;
				}
			}
			assert(geometry->model != NULL);

			vec3_from_string(geometry->position, strtok_r(NULL, token_separator, &line_state));
			vec3_from_string(geometry->scale, strtok_r(NULL, token_separator, &line_state));
			vec4_from_string(geometry->rotation, strtok_r(NULL, token_separator, &line_state));
		}
	}

	free(line);
	fclose(fp);

	return true;
}

void
world_area_unload(struct world *world, struct world_area *area) {
	free(area->geometry);
	area->geometry = NULL;
	area->num_geometry = 0;

	for (size_t i = 0; i < area->num_models; ++i) {
		model_store_release(world->model_store, area->models[i].model);
		free(area->models[i].name);
	}
	free(area->models);
	area->models = NULL;
	area->num_models = 0;
}

void
world_area_free(struct world *world, struct world_area *area) {
	world_area_unload(world, area);
	free(area->name);
	free(area->file);
}

bool
world_load(struct world *world, char *file) {
	const char *token_separator = " \n\r";
	size_t line_len = 0;
	char *line = NULL;
	FILE *fp = NULL;

	assert(world->model_store != NULL);

	world->file = assets_get_path(file, NULL);

	if (world->file == NULL) {
		return false;
	}

	fp = fopen(world->file, "r");

	if (!fp) {
		return false;
	}

	while (getline(&line, &line_len, fp) != -1) {
		char *line_state;
		char *token = strtok_r(line, token_separator, &line_state);

		if (!token)
			continue;

		/*  */ if (strcmp(token, "a") == 0) {
			// Area
			// a <name> <file> <position> <size> <rotation>
			world->areas = realloc(world->areas, sizeof(struct world_area) * (++world->num_areas));
			struct world_area *area = &world->areas[world->num_areas-1];

			memset(area, 0, sizeof(struct world_area));

			area->name = strdup(strtok_r(NULL, token_separator, &line_state));
			area->file = assets_get_path(strtok_r(NULL, token_separator, &line_state), world->file);

			vec3_from_string(area->position, strtok_r(NULL, token_separator, &line_state));
			vec3_from_string(area->size, strtok_r(NULL, token_separator, &line_state));
			quat_from_string(area->rotation, strtok_r(NULL, token_separator, &line_state));

			world_area_load(world, area);
		}
	}

	free(line);
	fclose(fp);

	return true;
}

void
world_free(struct world *world) {
	free(world->file);

	for (size_t i = 0; i < world->num_areas; ++i) {
		struct world_area *area = &world->areas[i];
		world_area_free(world, area);
	}

	free(world->areas);

	memset(world, 0, sizeof(struct world));
}

void
world_render(const struct world *world, struct object_render_queue *queue) {
	mat4 modelview_area;
	for (size_t i = 0; i < world->num_areas; ++i) {
		const struct world_area *area = &world->areas[i];

		mat4_translate(modelview_area, area->position);
		mat4_quat_into(modelview_area, modelview_area, area->rotation);

		for (size_t j = 0; j < area->num_geometry; ++j) {
			const struct geometry *geometry = &area->geometry[j];
			struct object_render_entry entry;

			mat4_translate_into(entry.modelview, modelview_area, geometry->position);
			mat4_quat_into(entry.modelview, entry.modelview, geometry->rotation);
			mat4_scale_into(entry.modelview, entry.modelview, geometry->scale);

			entry.dissolve = 0;
			entry.model = geometry->model;

			object_render_queue_push(queue, &entry);
		}
	}
}
