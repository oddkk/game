#include "window.h"
#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GLFW/glfw3.h>

static float window_get_dpi(GLFWwindow *wnd) {
	GLFWmonitor *monitor;
	const GLFWvidmode *mode;
	int physical_width, physical_height;

	monitor = glfwGetWindowMonitor(wnd);
	if (!monitor) {
		monitor = glfwGetPrimaryMonitor();
	}

	mode = glfwGetVideoMode(monitor);

	glfwGetMonitorPhysicalSize(monitor, &physical_width, &physical_height);

	return (float)mode->width / ((float)physical_width / 25.4f);

}

static void framebuffer_size_changed(GLFWwindow *wnd, int width, int height) {
	struct window *window = (struct window*)glfwGetWindowUserPointer(wnd);
	window->dpi = window_get_dpi(wnd);
	glViewport(0, 0, width, height);
	window->width = width;
	window->height = height;
}

static void glfw_error(int id, const char *msg) {
	fprintf(stderr, "glfw: (%i) %s\n", id, msg);
	exit(-1);
}

static bool init_glew() {
	GLenum err;

	glewExperimental = GL_TRUE;
	err = glewInit();
	if (err != GLEW_OK) {
		printf("glew: (%i) %s\n", err, glewGetErrorString(err));
		return false;
	}

	if ((err = glGetError()) != GL_NO_ERROR) {
		fprintf(stderr, "opengl: warning initializing glew (%i)\n", err);
	}

	return true;
}

bool window_init() {
	if (!glfwInit()) {
		fprintf(stderr, "glfw: Failed to initialize\n");
		return false;
	}

	glfwSetErrorCallback(glfw_error);

	return true;
}

void window_terminate() {
	glfwTerminate();
}

bool window_create(struct window *window) {
	GLFWwindow **wnd = (GLFWwindow**)&window->handle;
	GLFWmonitor *monitor = NULL;
	const GLFWvidmode* vmode;

	if (window->mode & WINDOW_MODE_FULLSCREEN) {
		monitor = glfwGetPrimaryMonitor();

		vmode = glfwGetVideoMode(monitor);

		glfwWindowHint(GLFW_RED_BITS, vmode->redBits);
		glfwWindowHint(GLFW_GREEN_BITS, vmode->greenBits);
		glfwWindowHint(GLFW_BLUE_BITS, vmode->blueBits);
		glfwWindowHint(GLFW_REFRESH_RATE, vmode->refreshRate);

		if (window->mode & WINDOW_MODE_WINDOWED || (window->width == 0 || window->height == 0)) {
			window->width = vmode->width;
			window->height = vmode->height;
		}
	}
	else if (window->width == 0 || window->height == 0) {
		window->width = 800;
		window->height = 600;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	*wnd = glfwCreateWindow(window->width, window->height, "Game", monitor, NULL);
	if (!*wnd) {
		fprintf(stderr, "Failed to initialize window\n");
		return false;
	}

	glfwSetWindowUserPointer(*wnd, window);

	window->dpi = window_get_dpi(*wnd);

	glfwSetFramebufferSizeCallback(*wnd, framebuffer_size_changed);

	glfwMakeContextCurrent(*wnd);
	GLenum err;

	if ((err = glGetError()) != GL_NO_ERROR) {
		fprintf(stderr, "opengl: error while creating window (%i)\n", err);
		return false;
	}

	if (!init_glew()) {
		return false;
	}
	const GLubyte *version = glGetString(GL_VERSION);
	fprintf(stderr, "OpenGL: %s\n", version);


	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	return true;
}

void window_free(struct window *window) {
	GLFWwindow *wnd = (GLFWwindow*)window->handle;
	glfwDestroyWindow(wnd);
	window->handle = NULL;
}

bool window_shouldclose(struct window *window) {
	GLFWwindow *wnd = (GLFWwindow*)window->handle;
	return glfwWindowShouldClose(wnd);
}

void window_swapbuffer(struct window *window) {
	GLFWwindow *wnd = (GLFWwindow*)window->handle;
	glfwSwapBuffers(wnd);
}

