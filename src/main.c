#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GLFW/glfw3.h>
#include <string.h>
#include <math.h>

#include "linmath.h"
#include "shader.h"
#include "window.h"
#include "model.h"
#include "model_store.h"
#include "framebuffer.h"
#include "timing.h"
#include "world.h"
#include "object_render.h"
#include "assets.h"
#include "lighting.h"
#include "font.h"
#include "ui.h"
#include "arguments.h"

#define static_array_size(array) (sizeof(array) / sizeof(array[0]))

struct program {
	struct window window;
};

void check_gl_error(const char *pos) {
	GLenum err;
	if ((err = glGetError()) != GL_NO_ERROR) {
		const GLubyte *message;
		message = gluErrorString(err);
		fprintf(stderr, "GL error (%s): %s\n", pos, message);
	}
}

int main(int argc, char *argv[]) {
	struct program program;
	struct arguments arguments = {};
	struct framebuffer pp = {};
	struct world wrld = {};
	struct object_render object_render = {};
	struct object_render_queue render_queue = {};
	struct lighting lighting = {};
	struct font_context font_context = {};
	struct font font_main = {};
	struct ui ui = {};
	model_store model_store = {};
	shader shdr, post_shdr;

	window_init();

	BEGIN_TIMED_BLOCK(initialize_program);

	arguments_parse(&arguments, argc, argv);

	program.window.mode = arguments.win_mode;
	program.window.width = arguments.width;
	program.window.height = arguments.height;


	BEGIN_TIMED_BLOCK(create_window);
	if (!window_create(&program.window)) {
		return -1;
	}
	glfwPollEvents();
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT |
			GL_DEPTH_BUFFER_BIT);
	window_swapbuffer(&program.window);
	END_TIMED_BLOCK(create_window);


	BEGIN_TIMED_BLOCK(load_font);
	if (!font_context_init(&font_context)) {
		fprintf(stderr, "Could not initialize font context\n");
		return -1;
	}
	font_context.ui = &ui;
	if (!font_from_asset(&font_context, &font_main, "/fonts/Varela-Regular.ttf", NULL)) {
		return -1;
	}
	END_TIMED_BLOCK(load_font);


	BEGIN_TIMED_BLOCK(load_shaders);
	if (!shader_from_file(&shdr, "/shaders/main.psh")) {
		fprintf(stderr, "Cannot load main shader\n");
		return -1;
	}

	if (!shader_from_file(&post_shdr, "/shaders/post.psh")) {
		fprintf(stderr, "Cannot load post shader\n");
		return -1;
	}
	END_TIMED_BLOCK(load_shaders);


	BEGIN_TIMED_BLOCK(load_world_render);
	if (!object_render_init(&object_render, &shdr, &ui)) {
		fprintf(stderr, "Could not initialize object renderer\n");
		return -1;
	}
	if (!object_render_queue_init(&render_queue)) {
		fprintf(stderr, "Could not initialize render queue\n");
		return -1;
	}
	END_TIMED_BLOCK(load_world_render);


	BEGIN_TIMED_BLOCK(load_world);
	wrld.model_store = &model_store;
	if (!world_load(&wrld, "/world/world.wld")) {
		fprintf(stderr, "Could not load world '%s'\n", "assets/world/world.wld");
		return -1;
	}
	END_TIMED_BLOCK(load_world);

	GLuint noise_grayscale;
	glGenTextures(1, &noise_grayscale);
	glBindTexture(GL_TEXTURE_2D, noise_grayscale);

	{
		char noise_data[256*256];
		for (size_t i = 0; i < (sizeof(noise_data)/sizeof(char)); ++i) {
			noise_data[i] = (char)(rand() % (0xff + 1));
		}

		glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, 256, 256, 0, GL_RED, GL_UNSIGNED_BYTE, noise_data);
	}

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glBindTexture(GL_TEXTURE_2D, 0);

	if (!lighting_init(&lighting, &shdr)) {
		fprintf(stderr, "Cannot initialize lighting\n");
		return -1;
	}

	vec3_set(lighting.data.in_light_point[0].position, (vec3){-1.5, 0.5, -1.5});
	vec3_set(lighting.data.in_light_point[0].diffuse, (vec3){0.6, 0.3, 0.0});
	vec3_set(lighting.data.in_light_point[0].specular, (vec3){0.6, 0.3, 0.0});

	vec3_set(lighting.data.in_light_point[1].position, (vec3){-7.5, 0.5, -1.5});
	vec3_set(lighting.data.in_light_point[1].diffuse, (vec3){0.6, 0.3, 0.0});
	vec3_set(lighting.data.in_light_point[1].specular, (vec3){0.6, 0.3, 0.9});

	vec3_set(lighting.data.in_light_box[0].point1, (vec3){-100, -100, -100});
	vec3_set(lighting.data.in_light_box[0].point2, (vec3){100, 100, 100});
	vec3_set(lighting.data.in_light_box[0].position, (vec3){75, 100, 100});
	vec3_set(lighting.data.in_light_box[0].ambient, (vec3){0.05, 0.05, 0.1});
	vec3_set(lighting.data.in_light_box[0].diffuse, (vec3){0.2, 0.2, 0.3});
	vec3_set(lighting.data.in_light_box[0].specular, (vec3){0.2, 0.2, 0.3});

	lighting_update(&lighting);

	framebuffer_init(&pp, &ui);
	framebuffer_bind_shader(&pp, &post_shdr);
	framebuffer_resize(&pp, (vec2i){program.window.width, program.window.height});

	ui_init(&ui);
	ui.dpi = program.window.dpi;

	vec3 cam_pos = { 0.0f, 7.0f, 5.0f };
	quat cam_rot;

	quat_axis_angles(cam_rot, deg_to_rad(30), (vec3){1,0,0});

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, noise_grayscale);
	glActiveTexture(GL_TEXTURE0);

	check_gl_error("post initialize");

	END_TIMED_BLOCK(initialize_program);

	vec2i framebuffer_size = {};
	float frame_duration = 0;
	while (!window_shouldclose(&program.window)) {
		float begin = glfwGetTime();

		if (program.window.width != framebuffer_size[0] || program.window.height != framebuffer_size[1]) {
			vec2i_set(framebuffer_size, (vec2i){program.window.width, program.window.height});
			object_render_resize(&object_render, framebuffer_size);
			framebuffer_resize(&pp, framebuffer_size);
			vec2i_set(ui.display_size, framebuffer_size);
			ui.dpi = program.window.dpi;
		}

		object_render_queue_clear(&render_queue);

		vec2 key_input = {};
		if (glfwGetKey(program.window.handle, GLFW_KEY_H) == GLFW_PRESS) {
			key_input[0] -= 1.0;
		}
		if (glfwGetKey(program.window.handle, GLFW_KEY_L) == GLFW_PRESS) {
			key_input[0] += 1.0;
		}

		if (glfwGetKey(program.window.handle, GLFW_KEY_J) == GLFW_PRESS) {
			key_input[1] += 1.0;
		}
		if (glfwGetKey(program.window.handle, GLFW_KEY_K) == GLFW_PRESS) {
			key_input[1] -= 1.0;
		}

		vec2_scale(key_input, key_input, 0.5);

		cam_pos[0] += key_input[0];
		cam_pos[2] += key_input[1];

		vec3_set(render_queue.camera_position, cam_pos);
		quat_set(render_queue.camera_rotation, cam_rot);

		lighting_update(&lighting);

		framebuffer_begin_render(&pp);

		glClear(GL_COLOR_BUFFER_BIT |
		        GL_DEPTH_BUFFER_BIT);

		world_render(&wrld, &render_queue);
		object_render_queue_render(&object_render, &render_queue, &program.window);

		framebuffer_end_render(&pp);
		framebuffer_render(&pp);

		font_printf(&font_main, 12, (vec2){0,0},
		            "%0.2fms/frame (%0.1ffps)\nrender queue: %lu",
		            frame_duration * 1000, 1.0f/frame_duration, render_queue.length);

		check_gl_error("post frame");

		window_swapbuffer(&program.window);
		glfwPollEvents();

		frame_duration = glfwGetTime() - begin;
	}

	glClear(GL_COLOR_BUFFER_BIT);

	BEGIN_TIMED_BLOCK(unload_program);

	world_free(&wrld);
	model_store_free(&model_store);
	object_render_queue_free(&render_queue);
	object_render_free(&object_render);
	font_free(&font_main);
	font_context_free(&font_context);
	framebuffer_free(&pp);
	shader_free(&shdr);
	shader_free(&post_shdr);
	window_free(&program.window);

	END_TIMED_BLOCK(unload_program);

	window_terminate();
	return 0;
}
