#define DARRAY(type) typedef struct {	  \
		type *data; \
		size_t size; \
		size_t capacity; \
	} array_##type; \
	size_t array_##type##_append(array_##type *arr, type *e) { \
		if (arr->size + 1 >= arr->capacity) { \
			arr->capacity = ((arr->size + (arr->size / 2)) / 2 + 1) * 2; \
			arr->data = realloc(arr->data, arr->capacity * sizeof(type)); \
			if (arr->data == 0) exit(-1); \
		} \
		memcpy(&arr->data[arr->size++], e, sizeof(type)); \
		return arr->size - 1; \
	} \
	void array_##type##_clear(array_##type *arr) { \
		arr->size = 0; \
	}

