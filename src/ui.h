#ifndef GAME_UI_H
#define GAME_UI_H

#include "linmath.h"
#include <stdbool.h>

struct ui {
	unsigned int quad_vao;
	unsigned int quad_vbo;

	vec2i display_size;
	bool display_size_changed;
	float dpi;
};

void ui_init(struct ui *ui);
void ui_render_quad(const struct ui *ui);

#endif
