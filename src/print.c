#include "print.h"
#include "utf8.h"
#include <stdbool.h>
#include <assert.h>
#include <float.h>
#include <math.h>
#include <string.h>

#include <stdio.h>
#include <stdlib.h>

static bool
is_digit(uint32_t c) {
	return c > '0' && c < '9';
}

static uint32_t
get_digit(uint32_t c) {
	return (c - '0');
}

static int32_t
read_int32(char *str, uint32_t *c, int *i) {
	int32_t res = 0;
	do {
		if (is_digit(*c)) {
			res = res * 10 + get_digit(*c);
		} else {
			break;
		}
	} while ((*c = u8_nextchar(str, i)));
	return res;
}

static double
read_float_from_va(va_list argp) {
	return va_arg(argp, double);
}

static int64_t
read_signed_number_from_va(va_list argp, uint32_t length) {
	switch (length) {
	case 1: {
		int32_t v = va_arg(argp, int32_t);
		return (int64_t)v;
	}
	case 2: {
		int32_t v = va_arg(argp, int32_t);
		return (int64_t)v;
	}
	case 4: {
		int32_t v = va_arg(argp, int32_t);
		return (int64_t)v;
	}
	case 8: {
		int64_t v = va_arg(argp, int64_t);
		return (int64_t)v;
	}
	default:
		assert(false);
	}
}

static uint64_t
read_unsigned_number_from_va(va_list argp, uint32_t length) {
	switch (length) {
	case 1: {
		uint32_t v = va_arg(argp, uint32_t);
		return (uint64_t)v;
	}
	case 2: {
		uint32_t v = va_arg(argp, uint32_t);
		return (uint64_t)v;
	}
	case 4: {
		uint32_t v = va_arg(argp, uint32_t);
		return (uint64_t)v;
	}
	case 8: {
		uint64_t v = va_arg(argp, uint64_t);
		return (uint64_t)v;
	}
	default:
		assert(false);
	}
}

struct number_to_string_result {
	uint32_t *str;
	size_t length;
};

static struct number_to_string_result
uint64_to_string(uint32_t *buffer, size_t buffer_len, uint64_t number, bool sign_always, uint8_t base, const char *base_chars, uint32_t precision) {
	struct number_to_string_result result;
	result.str = buffer + buffer_len;
	*(--result.str) = 0;
	while (number > 0) {
		*(--result.str) = base_chars[number % base];
		number /= base;
	}
	result.length = buffer + buffer_len - result.str;

	while (result.length <= precision) {
		*(--result.str) = '0';
		++result.length;
	}

	if (sign_always) {
		*(--result.str) = '+';
		++result.length;
	}
	
	return result;
}

static struct number_to_string_result
int64_to_string(uint32_t *buffer, size_t buffer_len, int64_t number, bool sign_always, bool sign_never, uint32_t base, const char *base_chars, uint32_t precision) {
	struct number_to_string_result result;
	bool negative = number < 0;
	if (negative) {
		number = -number;
	}
	result = uint64_to_string(buffer, buffer_len, (uint64_t)number, false, base, base_chars, precision);
	if (negative) {
		if (!sign_never) {
			*(--result.str) = '-';
		}
	} else {
		if (sign_always) {
			*(--result.str) = '+';
		}
	}

	result.length = (buffer + 50) - result.str;

	return result;
}

/*
  TODO: Implement this
static struct number_to_string_result
floating_point_to_string(uint32_t *buffer, size_t buffer_len, long double number, bool sign_always, bool sign_never, bool capital, uint32_t precision) {
}
*/

static struct number_to_string_result
floating_point_to_string(uint32_t *buffer, size_t buffer_len, double number, uint32_t precision) {
	struct number_to_string_result res;
	char frmt[100];
	char b[100];
	int len;
	(void)buffer_len;
	(void)precision;

	snprintf(frmt, sizeof(frmt) / sizeof(char), "%%.%if", precision);

	len = snprintf(b, sizeof(b) / sizeof(char), frmt, number);

	if (len < 0) {
		res.str = buffer;
		res.length = 0;
		return res;
	}

	for (int i = 0; i < len; ++i) {
		buffer[i] = b[i];
	}

	res.str = buffer;
	res.length = len;

	return res;	
}

static void
print_string(void(*cb)(uint32_t,void*), void *data, size_t *num_printed, uint32_t *str, size_t str_len, uint32_t pad, bool justify_left, uint32_t width) {
	if (justify_left) {
		for (size_t iter = 0; iter < str_len; ++iter) {
			cb(str[iter], data);
			++(*num_printed);
		}
		for (uint32_t i = str_len; i <= width; ++i) {
			cb(pad, data);
			++(*num_printed);
		}
	} else {
		for (uint32_t i = str_len; i <= width; ++i) {
			cb(pad, data);
			++(*num_printed);
		}
		for (size_t iter = 0; iter < str_len; ++iter) {
			cb(str[iter], data);
			++(*num_printed);
		}
	}
}

static const char base8[] = "01234567";
static const char base10[] = "0123456789";
static const char base16[] = "0123456789abcdef";
static const char base16_upper[] = "0123456789ABCDEF";

void
vprintf_cb(void(*cb)(uint32_t, void*), void *data, char *frmt, va_list argp) {
	(void)argp;
	int i = 0;
	uint32_t c;
	size_t num_printed = 0;
	while ((c = u8_nextchar(frmt, &i)) != 0) {
		if (c == '%') {
			bool justify_left = false;
			bool sign_always = false;
			bool sign_never = false;
			bool prefix = false;
			bool pad_left_0 = false;
			int32_t width = 0;
			int32_t precision = 0;
			bool precision_set = false;
			int32_t length = sizeof(int);

			c = u8_nextchar(frmt, &i);

			// Read flags
			do {
				/*  */ if (c == '-') {
					justify_left = true;
				} else if (c == '+') {
					sign_always = true;
				} else if (c == ' ') {
					sign_never = true;
				} else if (c == '#') {
					prefix = true;
				} else if (c == '0') {
					pad_left_0 = true;
				} else {
					break;
				}
			} while ((c = u8_nextchar(frmt, &i)) != 0);

			// Read width
			/*  */ if (c == '*') {
				width = va_arg(argp, int32_t);
				c = u8_nextchar(frmt, &i);
			} else if (is_digit(c)) {
				width = read_int32(frmt, &c, &i);
			}

			// Read precision
			if (c == '.') {
				c = u8_nextchar(frmt, &i);
				/*  */ if (c == '*') {
					precision = va_arg(argp, int32_t);
					c = u8_nextchar(frmt, &i);
					precision_set = true;
				} else if (is_digit(c)) {
					precision = read_int32(frmt, &c, &i);
					precision_set = true;
				}
			}

			// Read length
			/*  */ if (c == 'h') {
				c = u8_nextchar(frmt, &i);
				if (c == 'h') {
					length = sizeof(char);
					c = u8_nextchar(frmt, &i);
				} else {
					length = sizeof(short);
				}
			} else if (c == 'l') {
				length = sizeof(long);
				c = u8_nextchar(frmt, &i);
			} else if (c == 'j') {
				length = sizeof(intmax_t);
				c = u8_nextchar(frmt, &i);
			} else if (c == 'z') {
				length = sizeof(size_t);
				c = u8_nextchar(frmt, &i);
			} else if (c == 't') {
				length = sizeof(ptrdiff_t);
				c = u8_nextchar(frmt, &i);
			}

			uint32_t buffer[50];

			switch (c) {
			case 'd':
			case 'i': {
				struct number_to_string_result str;
				int64_t v = read_signed_number_from_va(argp, length);
				str = int64_to_string(buffer, 50, v, sign_always, sign_never, 10, base10, precision);
				print_string(cb, data, &num_printed, str.str, str.length, pad_left_0 ? '0' : ' ', justify_left, width);
			} break;
			case 'u': {
				struct number_to_string_result str;
				int64_t v = read_unsigned_number_from_va(argp, length);
				str = uint64_to_string(buffer, 50, v, sign_always, 10, base10, precision);
				print_string(cb, data, &num_printed, str.str, str.length, pad_left_0 ? '0' : ' ', justify_left, width);
			} break;
			case 'o': {
				struct number_to_string_result str;
				int64_t v = read_unsigned_number_from_va(argp, length);
				str = uint64_to_string(buffer, 50, v, sign_always, 8, base8, precision);
				if (prefix) {
					*(--str.str) = '0';
					++str.length;
				}
				print_string(cb, data, &num_printed, str.str, str.length, pad_left_0 ? '0' : ' ', justify_left, width);
			} break;
			case 'x': {
				struct number_to_string_result str;
				int64_t v = read_unsigned_number_from_va(argp, length);
				str = uint64_to_string(buffer, 50, v, sign_always, 16, base16, precision);
				if (prefix) {
					*(--str.str) = 'x';
					++str.length;
					*(--str.str) = '0';
					++str.length;
				}
				print_string(cb, data, &num_printed, str.str, str.length, pad_left_0 ? '0' : ' ', justify_left, width);
			} break;
			case 'X': {
				struct number_to_string_result str;
				int64_t v = read_unsigned_number_from_va(argp, length);
				str = uint64_to_string(buffer, 50, v, sign_always, 16, base16_upper, precision);
				if (prefix) {
					*(--str.str) = 'X';
					++str.length;
					*(--str.str) = '0';
					++str.length;
				}
				print_string(cb, data, &num_printed, str.str, str.length, pad_left_0 ? '0' : ' ', justify_left, width);
			} break;
			case 'f':
			case 'F': {
				struct number_to_string_result str;
				double v = read_float_from_va(argp);
				str = floating_point_to_string(buffer, 50, v, precision);
				print_string(cb, data, &num_printed, str.str, str.length, pad_left_0 ? '0' : ' ', justify_left, width);
			} break;
			case 'e': {
				double v = read_float_from_va(argp);
				(void)v;
			} break;
			case 'E': {
				double v = read_float_from_va(argp);
				(void)v;
			} break;
			case 'g': {
				double v = read_float_from_va(argp);
				(void)v;
			} break;
			case 'G': {
				double v = read_float_from_va(argp);
				(void)v;
			} break;
			case 'a': {
				double v = read_float_from_va(argp);
				(void)v;
			} break;
			case 'A': {
				double v = read_float_from_va(argp);
				(void)v;
			} break;
			case 'c': {
				print_string(cb, data, &num_printed, &c, 1, pad_left_0 ? '0' : ' ', justify_left, width);
			} break;
			case 's': {
				char *str = va_arg(argp, char*);
				uint32_t *buf_p = buffer;
				int str_i = 0;
				uint32_t str_c;
				int32_t len = 0;
				while ((str_c = u8_nextchar(str, &str_i)) != 0 && (!precision_set || len < precision)) {
					*(buf_p++) = str_c;
					++len;
				}
				print_string(cb, data, &num_printed, buffer, len, pad_left_0 ? '0' : ' ', justify_left, width);
			} break;
			case 'p': {
				struct number_to_string_result str;
				void *v = va_arg(argp, void*);
				str = uint64_to_string(buffer, 50, (size_t)v, sign_always, 16, base16_upper, precision);
				if (prefix) {
					*(--str.str) = 'x';
					++str.length;
					*(--str.str) = '0';
					++str.length;
				}
				print_string(cb, data, &num_printed, str.str, str.length, pad_left_0 ? '0' : ' ', justify_left, width);
			}break;
			case 'n': {
				switch (length) {
				case 1: {
					uint8_t *v = va_arg(argp, uint8_t*);
					*v = num_printed;
				}
				case 2: {
					uint16_t *v = va_arg(argp, uint16_t*);
					*v = num_printed;
				}
				case 4: {
					uint32_t *v = va_arg(argp, uint32_t*);
					*v = num_printed;
				}
				case 8: {
					uint64_t *v = va_arg(argp, uint64_t*);
					*v = num_printed;
				}
				default:
					assert(false);
				}
			} break;
			case '%':
				cb('%', data);
				++num_printed;
				break;
			}
		} else {
			cb(c, data);
			++num_printed;
		}
	}
}
