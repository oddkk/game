#ifndef GAME_WORLD_H
#define GAME_WORLD_H

#include <stdbool.h>
#include <stddef.h>
#include "linmath.h"
#include "model.h"
#include "model_store.h"

struct geometry {
	vec3 position;
	vec3 scale;
	quat rotation;
	model *model;
};

struct world_area {
	char *name;
	char *file;

	// Position is where in world-coord the origo of the area is
	vec3 position;
	vec3 size;
	quat rotation;

	struct world_asset_model {
		char *name;
		model *model;
	} *models;
	size_t num_models;
	struct geometry *geometry;
	size_t num_geometry;
};

struct world_node {
	struct world_node *parent;
	bool leaf;
	union {
		struct world_node *children[8];
		struct geometry *geometry;
	};
	aabb box;
};

struct world {
	char *file;
	struct world_node root;
	struct world_area *areas;
	unsigned int num_areas;

	model_store *model_store;
};

struct object_render_queue;

bool world_load(struct world *world, char *file);
void world_free(struct world *world);
void world_render(const struct world *world, struct object_render_queue *queue);

#endif
