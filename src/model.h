#ifndef GAME_MODEL_H
#define GAME_MODEL_H

#include <stdbool.h>
#include "linmath.h"

struct model_mesh {
	unsigned int vao, vbo;
	unsigned int num_vertecies;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float specular_exp;
	aabb bounding_box;
};
typedef struct {
	struct model_mesh *meshes;
	unsigned int num_meshes;
	aabb bounding_box;
	bool loaded;
} model;

typedef struct {
	int in_ambient;
	int in_diffuse;
	int in_specular;
	int in_specular_exp;
} model_shader;

bool model_load_from_file(model *mdl, const char *path);
void model_unload(model *mdl);
void model_draw(const model *mdl, const model_shader *shdr);
model_shader model_make_model_shader(unsigned int program);

#endif
