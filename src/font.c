#include "font.h"
#include "assets.h"
#include "print.h"
#include <stdio.h>
#include <stdlib.h>
#include <ft2build.h>
#include FT_FREETYPE_H
#include <GL/glew.h>
#include <assert.h>

void check_gl_error(const char *pos);

bool
font_context_init(struct font_context *lib) {
	FT_Library *ft_lib = (FT_Library*)&lib->library_handle;
	FT_Error error = 0;

	if (!shader_from_file(&lib->font_shader, "/shaders/font.psh")) {
		fprintf(stderr, "Could not load font shader\n");
		return false;
	}

	lib->uniform_screen_size
		= glGetUniformLocation(lib->font_shader.program, "in_screen_size");
	lib->uniform_atlas
		= glGetUniformLocation(lib->font_shader.program, "in_atlas");
	lib->uniform_atlas_size
		= glGetUniformLocation(lib->font_shader.program, "in_atlas_size");
	lib->uniform_glyph_pos
		= glGetUniformLocation(lib->font_shader.program, "in_glyph_pos");
	lib->uniform_glyph_size
		= glGetUniformLocation(lib->font_shader.program, "in_glyph_size");
	lib->uniform_glyph_bearing
		= glGetUniformLocation(lib->font_shader.program, "in_glyph_bearing");
	lib->uniform_cursor
		= glGetUniformLocation(lib->font_shader.program, "in_cursor");
	lib->uniform_matrix
		= glGetUniformLocation(lib->font_shader.program, "in_matrix");
	lib->uniform_color
		= glGetUniformLocation(lib->font_shader.program, "in_color");

	error = FT_Init_FreeType(ft_lib);
	if (error != 0) {
		*ft_lib = NULL;
		fprintf(stderr, "Could not initialize FreeType2\n");
		return false;
	}

	return true;
}

void font_context_free(struct font_context *context) {
	FT_Library *ft_lib = (FT_Library*)&context->library_handle;

	FT_Done_FreeType(*ft_lib);
	*ft_lib = 0;
}

bool font_from_file(struct font_context *context, struct font *face, const char *path) {
	FT_Library *ft_lib = (FT_Library*)&context->library_handle;
	FT_Face *ft_face = (FT_Face*)&face->face_handle;
	FT_Error error = 0;

	face->context = context;

	error = FT_New_Face(*ft_lib, path, 0, ft_face);
	if (error != 0) {
		*ft_face = NULL;
		fprintf(stderr, "Could not load font file %s\n", path);
		return false;
	}

	return true;
}

bool font_from_asset(struct font_context *lib, struct font *face, const char *asset, const char *owner) {
	char *path = assets_get_path(asset, owner);
	if (!path) {
		fprintf(stderr, "Cannot find asset %s\n", asset);
		return false;
	}

	bool res = font_from_file(lib, face, path);
	free(path);
	return res;
}

struct font_size *font_get_size(struct font *face, float size) {
	FT_Face *ft_face = (FT_Face*)&face->face_handle;
	int dpi = (int)face->context->ui->dpi;
	int pt = (int)(size * 64); // In pt/64
	struct font_size *s;
	FT_Error error = 0;

	// Check if the font size was already created
	for (size_t i = 0; i < face->sizes_num; ++i) {
		if ((int)(face->sizes[i].size * 64) == pt) {
			return &face->sizes[i];
		}
	}

	// Try reallocate size for the new font size
	struct font_size *tmp_sizes = realloc(face->sizes, sizeof(struct font_size) * ++face->sizes_num);
	if (!tmp_sizes) {
		fprintf(stderr, "Could not allocate memory for face size\n");
		return NULL;
	}
	face->sizes = tmp_sizes;

	s = &face->sizes[face->sizes_num - 1];
	memset(s, 0, sizeof(struct font_size));

	s->size = size;

	// Get line height
	error = FT_Set_Char_Size(*ft_face, 0, pt, 0, dpi);
	if (error != 0) {
		fprintf(stderr, "Failed to set char size\n");
		return NULL;
	}
	s->line_height = (float)(*ft_face)->size->metrics.height / 64.0f;

	glGenTextures(1, &s->atlas_texture);
	glBindTexture(GL_TEXTURE_2D, s->atlas_texture);

	vec2i_set(s->atlas_size, (vec2i){ FONT_ATLAS_WIDTH, FONT_ATLAS_WIDTH });

	s->atlas = calloc(s->atlas_size[0]*s->atlas_size[1], sizeof(unsigned char));

	glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, s->atlas_size[0],s->atlas_size[1], 0, GL_RED, GL_UNSIGNED_BYTE, s->atlas);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glBindTexture(GL_TEXTURE_2D, 0);

	s->glyphs_capacity = FONT_ATLAS_CAPACITY_INCREMENT;
	s->glyphs = calloc(s->glyphs_capacity, sizeof(struct font_glyph));
	s->glyphs_num = 0;

	return s;
}

static void
sub_image(char *dest, const vec2i dest_begin, const vec2i dest_size,
          const char *src, const vec2i src_begin, const vec2i src_size,
          const mat2i size, const mat3i transform) {
	for (size_t i = 0; i < (size_t)size[0] * (size_t)size[1]; ++i) {
		vec3i pos = { i % size[0], i / size[0], 1 };
		vec2i dest_pos, src_pos;
		size_t dest_i, src_i;

		vec2i_add(src_pos, src_begin, pos);

		mat3i_vec3i_multiply(pos, transform, pos);
		vec2i_add(dest_pos, dest_begin, pos);

		dest_i = dest_pos[0] + dest_pos[1] * dest_size[0];
		src_i = src_pos[0] + src_pos[1] * src_size[0];

		dest[dest_i] = src[src_i];
	}
}

struct find_position_result {
	size_t x, y;
};

static struct find_position_result surface_find_best_position(const size_t *surface, size_t width) {
	struct find_position_result res = {
		.x=0, .y=SIZE_MAX
	};
	for (size_t i = 0; i < FONT_ATLAS_WIDTH - width; ++i) {
		size_t max = 0;
		for (size_t j = i; j < i + width; ++j) {
			if (surface[j] > max) {
				max = surface[j];
			}
		}
		if (max < res.y) {
			res.x = i;
			res.y = max;
		}
	}
	return res;
}

static void surface_occupy(size_t *surface, size_t x, size_t width, size_t height) {
	for (size_t i = x; i < x + width; ++i) {
		surface[i] = height;
	}
}

struct font_glyph *font_get_glyph(struct font *face, unsigned int character, float size) {
	struct font_size *s = font_get_size(face, size);
	return font_size_get_glyph(face, s, character);
}

struct font_glyph *font_size_get_glyph(struct font *face, struct font_size *s, unsigned int character) {
	FT_Face *ft_face = (FT_Face*)&face->face_handle;
	FT_Error error = 0;

	if (character >= ' ' && character <= '~') {
		if (s->default_glyphs[character-' '].initialized) {
			return &s->default_glyphs[character-' '].glyph;
		}
	} else {
		for (size_t i = 0; i < s->glyphs_num; ++i) {
			if (s->glyphs[i].character == character) {
				return &s->glyphs[i];
			}
		}
	}

	int pt = (int)(s->size * 64.0f);
	int dpi = (int)face->context->ui->dpi;

	error = FT_Set_Char_Size(*ft_face, 0, pt, 0, dpi);

	if (error != 0) {
		fprintf(stderr, "Failed to set char size\n");
		return NULL;
	}

	error = FT_Load_Char(*ft_face, character, FT_LOAD_RENDER);

	if (error != 0) {
		fprintf(stderr, "Could not load character %c\n", (char)character);
		return NULL;
	}

	vec2i glyph_size = {
		(*ft_face)->glyph->bitmap.width,
		(*ft_face)->glyph->bitmap.rows
	};
	vec2i glyph_size_border;

	// We add 2 to introduce a 1-pixel border
	const int border_width = 2;
	vec2i_add(glyph_size_border, glyph_size, (vec2i){border_width * 2,border_width * 2});
	//int glyph_space_y = (rotated ? width : height);

	struct find_position_result best_position;

	best_position = surface_find_best_position(s->surface, (size_t)glyph_size_border[0]);

	surface_occupy(s->surface, best_position.x, glyph_size_border[0], best_position.y + glyph_size_border[1]);

	vec2i glyph_position = {
		best_position.x,
		best_position.y
	};

	vec2i glyph_inner_position;
	vec2i_add(glyph_inner_position, glyph_position, (vec2i){border_width, border_width});

	bool full_upload = false;

	if (glyph_position[1] + glyph_size_border[1] > s->atlas_size[1]) {
		// The glyph falls outside the texture. Resize
		int old_size = s->atlas_size[1];
		s->atlas_size[1] += FONT_ATLAS_WIDTH;
		unsigned char *tmp = realloc(s->atlas, s->atlas_size[0] * s->atlas_size[1] * sizeof(char));
		if (!tmp) {
			fprintf(stderr, "Could not allocate more space for glyph.\n");
			return NULL;
		}
		s->atlas = tmp;
		memset(&s->atlas[old_size * s->atlas_size[0]], 0,
		       (s->atlas_size[1] - old_size) * s->atlas_size[0]);
		full_upload = true;
	}

	vec2i glyph_dest_size = VEC2(glyph_size);

	mat3i mat_normal = {
		1, 0, 0,
		0,-1, 0,
		0, glyph_dest_size[1]-1, 1
	};

	sub_image((char*)s->atlas, glyph_inner_position, s->atlas_size,
	          (char*)(*ft_face)->glyph->bitmap.buffer, (vec2i){0,0}, glyph_size,
	          glyph_size, mat_normal);

	check_gl_error("pre font tex upload");

	glBindTexture(GL_TEXTURE_2D, s->atlas_texture);

	full_upload = true;

	// TODO: Implement upload of glyph only
	if (!full_upload) {
		//glTexSubImage2D(GL_TEXTURE_2D, 0, x_offset+1, y_offset+1, width-2, height-2, GL_RED, GL_UNSIGNED_BYTE, s->atlas);
		glTexSubImage2D(GL_TEXTURE_2D, 0,
		                glyph_position[0] + border_width, glyph_position[1] + border_width,
		                glyph_size[0], glyph_size[1],
		                GL_RED, GL_UNSIGNED_BYTE, s->atlas);//(*ft_face)->glyph->bitmap.buffer);
		check_gl_error("post partial font tex upload");
	} else {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, s->atlas_size[0], s->atlas_size[1], 0, GL_RED, GL_UNSIGNED_BYTE, s->atlas);
		check_gl_error("post full font tex upload");
	}


	glBindTexture(GL_TEXTURE_2D, 0);

	struct font_glyph *glyph = NULL;

	if (character >= ' ' && character <= '~') {
		s->default_glyphs[character-' '].initialized = true;
		glyph = &s->default_glyphs[character-' '].glyph;
	} else {
		if (s->glyphs_num+1 >= s->glyphs_capacity) {
			s->glyphs_capacity += FONT_ATLAS_CAPACITY_INCREMENT;
			struct font_glyph *tmp_glyphs = realloc(s->glyphs, sizeof(struct font_glyph) * s->glyphs_capacity);
			if (!tmp_glyphs) {
				fprintf(stderr, "Could not allocate memory for face glyphs\n");
				return NULL;
			}
			s->glyphs = tmp_glyphs;
		}
		glyph = &s->glyphs[s->glyphs_num++];
	}

	glyph->character = character;

	vec2i_set(glyph->atlas_position, glyph_inner_position);
	vec2i_set(glyph->size, glyph_size);
	vec2_set(glyph->advance, (vec2){
			(float)((*ft_face)->glyph->metrics.horiAdvance >> 6),
			(float)((*ft_face)->glyph->metrics.vertAdvance >> 6)
	});
	vec2_set(glyph->bearing, (vec2){
			(float)((*ft_face)->glyph->metrics.horiBearingX >> 6),
			(float)((*ft_face)->glyph->metrics.horiBearingY >> 6)
	});

	return glyph;
}

struct font_printf_cb_t {
	struct font *face;
	struct font_size *face_size;
	vec4 color;
	vec2 cursor;
	float size;
	int state;
	uint32_t control[100];
	uint32_t *control_c;
};

#define FONT_PRINTF_STATE_PRINT (0)
#define FONT_PRINTF_STATE_CONTROL_BEGIN (1)
#define FONT_PRINTF_STATE_CONTROL (2)

static void
print_character(uint32_t c, struct font_printf_cb_t *data) {
	struct font_glyph *glyph = font_size_get_glyph(data->face, data->face_size, c);

	glUniform2iv(data->face->context->uniform_glyph_pos, 1,
				 glyph->atlas_position);

	glUniform2iv(data->face->context->uniform_glyph_size, 1,
				 glyph->size);

	glUniform2fv(data->face->context->uniform_glyph_bearing, 1,
				 glyph->bearing);

	glUniform2fv(data->face->context->uniform_cursor, 1,
				 data->cursor);

	ui_render_quad(data->face->context->ui);
	data->cursor[0] += glyph->advance[0];
}

static uint32_t*
u32strfind(uint32_t *haystack, uint32_t needle) {
	do {
		if (*haystack == needle) {
			return haystack;
		}
		++haystack;
	} while (*haystack != 0);
	return NULL;
}

static bool
u32u8strcmp(uint32_t *str1, char *str2) {
	while (*str1 != 0 && *str2 != 0) {
		if (*str1++ != (uint32_t)*str2++) {
			return false;
		}
	}
	return *str1 == (uint32_t)*str2;
}

static size_t
u32strlen(uint32_t *str) {
	size_t res = 0;
	while (*(str++) != 0) res++;
	return res;
}

static uint32_t
char_to_hex(uint32_t c) {
	if (c >= '0' && c <= '9') {
		return c - '0';
	} else if (c >= 'a' && c <= 'f') {
		return 10 + (c - 'a');
	} else if (c >= 'A' && c <= 'F') {
		return 10 + (c - 'a');
	} else {
		return 0;
	}
}

static uint32_t
next_hex(uint32_t *str, size_t n) {
	uint32_t res = 0;
	while (n-- > 0) {
		res *= 16;
		res += char_to_hex(*(str++));
	}
	return res;
}

static void
parse_color(vec3 dest, uint32_t *str) {
	if (*str == '#') {
		size_t len = u32strlen(++str);
		/*  */ if (len == 3) { // RGB
			for (size_t i = 0; i < 3; ++i) {
				dest[i] = (float)next_hex(str, 1) / 15.0f;
				str += 1;
			}
			dest[3] = 1;
		} else if (len == 4) { // RGBA
			for (size_t i = 0; i < 4; ++i) {
				dest[i] = (float)next_hex(str, 1) / 15.0f;
				str += 1;
			}
		} else if (len == 6) { // RRGGBB
			for (size_t i = 0; i < 3; ++i) {
				dest[i] = (float)next_hex(str, 2) / 255.0f;
				str += 2;
			}
			dest[3] = 1;
		} else if (len == 8) { // RRGGBBAA
			for (size_t i = 0; i < 4; ++i) {
				dest[i] = (float)next_hex(str, 2) / 255.0f;
				str += 2;
			}
		}
	}
}

static void
eval_controlstr(uint32_t *str, struct font_printf_cb_t *data) {
	uint32_t *key = str;
	uint32_t *value = u32strfind(str, ':');
	if (value != NULL) {
		*(value++) = 0;
	}

	if (u32u8strcmp(key, "color")) {
		parse_color(data->color, value);
		glUniform4fv(data->face->context->uniform_color, 1,
		             data->color);
	}
}

void font_printf_cb(uint32_t c, void *dt) {
	struct font_printf_cb_t *data = (struct font_printf_cb_t*)dt;
	switch (data->state) {
	case FONT_PRINTF_STATE_PRINT: {
		if (c < 32) {
			switch (c) {
			case '\n': {
				data->cursor[0] = 0;
				data->cursor[1] = data->cursor[1] - data->face_size->line_height;
			} break;
			}
		} else if (c == '$') {
			data->state = FONT_PRINTF_STATE_CONTROL_BEGIN;
		} else {
			print_character(c, data);
		}
	} break;
	case FONT_PRINTF_STATE_CONTROL_BEGIN: {
		if (c == '$') {
			print_character(c, data);
			data->state = FONT_PRINTF_STATE_PRINT;
		} else {
			data->state = FONT_PRINTF_STATE_CONTROL;
			data->control_c = data->control;
			*(data->control_c++) = c;
			*data->control_c = 0;
		}
	} break;
	case FONT_PRINTF_STATE_CONTROL: {
		if (c == '$') {
			eval_controlstr(data->control, data);
			data->state = FONT_PRINTF_STATE_PRINT;
		} else if (c == ';') {
			eval_controlstr(data->control, data);
			data->control_c = data->control;
			*data->control_c = 0;
		} else {
			*(data->control_c++) = c;
			*data->control_c = 0;
		}
	} break;
	}
}

static void font_vprintf_mat(struct font *face, float size, mat3 matrix, char *frmt, va_list argp) {
	struct font_printf_cb_t data = {
		.face = face,
		.size = size
	};
	data.face_size = font_get_size(face, size);

	vec2_set(data.cursor, (vec2){0, -data.face_size->line_height});

	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glActiveTexture(GL_TEXTURE0);
	glUseProgram(face->context->font_shader.program);
	glBindTexture(GL_TEXTURE_2D, data.face_size->atlas_texture);

	glUniform1i(face->context->uniform_atlas, 0),

	glUniform2iv(face->context->uniform_atlas_size, 1,
					data.face_size->atlas_size);

	glUniform4f(face->context->uniform_color, 1, 1, 1, 1);

	glUniform2iv(face->context->uniform_screen_size, 1,
				 data.face->context->ui->display_size);

	glUniformMatrix3fv(face->context->uniform_matrix, 1,
					   GL_TRUE, matrix);

	vprintf_cb(font_printf_cb, &data, frmt, argp);

	glBindTexture(GL_TEXTURE_2D, 0);
	glUseProgram(0);

	glDisable(GL_BLEND);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
}

void font_printf_mat(struct font *face, float size, mat3 matrix, char *frmt, ...) {
	va_list argp;

	va_start(argp, frmt);
	font_vprintf_mat(face, size, matrix, frmt, argp);
	va_end(argp);
}

void font_printf(struct font *face, float size, vec2 position, char *frmt, ...) {
	vec2 translation;
	va_list argp;
	mat3 matrix;

	vec2_set(translation, (vec2){
		position[0],
		face->context->ui->display_size[1] - position[1]
	});

	mat3_translate(matrix, translation);

	va_start(argp, frmt);
	font_vprintf_mat(face, size, matrix, frmt, argp);
	va_end(argp);
}

void font_size_free(struct font_size *size) {
	size->size = 0;

	glDeleteTextures(1, &size->atlas_texture);
	size->atlas_texture = 0;
	
	free(size->atlas);
	size->atlas = NULL;
	vec2i_set(size->atlas_size, (vec2i){0,0});
	free(size->glyphs);
	size->glyphs = NULL;
	size->glyphs_num = 0;

	memset(size->surface, 0, FONT_ATLAS_WIDTH * sizeof(size_t));
}

void font_free(struct font *face) {
	FT_Face *ft_face = (FT_Face*)&face->face_handle;

	FT_Done_Face(*ft_face);

	for (size_t i = 0; i < face->sizes_num; ++i) {
		font_size_free(&face->sizes[i]);
	}

	free(face->sizes);
	face->sizes = NULL;
	face->sizes_num = 0;
}

