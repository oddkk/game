#include "framebuffer.h"
#include "shader.h"
#include "linmath.h"
#include <GL/glew.h>
#include <GL/gl.h>
#include <stdio.h>

void check_gl_error(const char*);

static const vec4 fbo_plane_indecies[] = {
	{ -1, -1, 0, 0 },
	{ -1,  1, 0, 1 },
	{  1,  1, 1, 1 },

	{ -1, -1, 0, 0 },
	{  1, -1, 1, 0 },
	{  1,  1, 1, 1 },
};

static void
_create_quad(unsigned int *vao, unsigned int *buffer) {
	glGenVertexArrays(1, vao);
	glBindVertexArray(*vao);

	glGenBuffers(1, buffer);
	glBindBuffer(GL_ARRAY_BUFFER, *buffer);

	glBufferData(GL_ARRAY_BUFFER, sizeof(fbo_plane_indecies), fbo_plane_indecies, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(vec4), (void*)0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(vec4), (void*)sizeof(vec2));

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

static int
_create_framebuffer(unsigned int width, unsigned int height,
					unsigned int *fbo, unsigned int *fbo_tex, unsigned int *fbo_depth) {
	glGenFramebuffers(1, fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, *fbo);

	glGenTextures(1, fbo_tex);
	glBindTexture(GL_TEXTURE_2D, *fbo_tex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glGenRenderbuffers(1, fbo_depth);
	glBindRenderbuffer(GL_RENDERBUFFER, *fbo_depth);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);

	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, *fbo_depth);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, *fbo_tex, 0);

	GLenum draw_buffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, draw_buffers);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		return -1;
	}

	glBindTexture(GL_TEXTURE_2D, 0);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return 0;
}

void
framebuffer_init(struct framebuffer *fb, struct ui *ui) {
	_create_quad(&fb->fbo_plane_vao, &fb->fbo_plane_buffer);
	fb->ui = ui;
}

void
framebuffer_bind_shader(struct framebuffer *fb, shader *shdr) {
	if (shdr == 0) {
		fb->shader = 0;
		fb->uniform.framebuffer = 0;
		fb->uniform.screen_size = 0;
		return;
	}
	fb->shader = shdr;

	fb->uniform.framebuffer = glGetUniformLocation(fb->shader->program, "in_framebuffer");
	fb->uniform.screen_size = glGetUniformLocation(fb->shader->program, "in_screensize");
}

void
framebuffer_resize(struct framebuffer *fb, vec2i size) {
	vec2i_set(fb->target_size, size);

	glDeleteRenderbuffers(1, &fb->fbo_depth);
	glDeleteFramebuffers(1, &fb->fbo);
	glDeleteTextures(1, &fb->fbo_tex);

	if (_create_framebuffer(fb->target_size[0], fb->target_size[1], &fb->fbo, &fb->fbo_tex, &fb->fbo_depth) != 0) {
		// Failed
		fprintf(stderr, "Failed to create framebuffer\n");
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0, 0, fb->ui->display_size[0], fb->ui->display_size[1]);
		fb->fbo = 0;
		fb->fbo_tex = 0;
		fb->fbo_depth = 0;
	}
}

void
framebuffer_begin_render(const struct framebuffer *fb) {
	if (fb->fbo == 0) {
		return;
	}
	glBindFramebuffer(GL_FRAMEBUFFER, fb->fbo);
	glViewport(0, 0, fb->target_size[0], fb->target_size[1]);
}

void
framebuffer_end_render(const struct framebuffer *fb) {
	if (fb->fbo == 0) {
		return;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, fb->ui->display_size[0], fb->ui->display_size[1]);
}

void
framebuffer_render(const struct framebuffer *fb) {
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, fb->fbo_tex);

	glBindVertexArray(fb->fbo_plane_vao);

	glUseProgram(fb->shader->program);

	glUniform2f(fb->uniform.screen_size, fb->target_size[0], fb->target_size[1]);
	glUniform1i(fb->uniform.framebuffer, 0);

	glDrawArrays(GL_TRIANGLES, 0, 6);

	glUseProgram(0);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
}

void
framebuffer_free(struct framebuffer *fb) {
	glDeleteBuffers(1, &fb->fbo_plane_buffer);
	glDeleteVertexArrays(1, &fb->fbo_plane_vao);
	glDeleteRenderbuffers(1, &fb->fbo_depth);
	glDeleteFramebuffers(1, &fb->fbo);
	glDeleteTextures(1, &fb->fbo_tex);
}
