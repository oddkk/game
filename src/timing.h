#include <stdbool.h>

struct timed_block {
	double begin;
	double end;
	bool is_running;
	char *name;
};

void begin_timed_block(struct timed_block *block, const char *name);
void end_timed_block(struct timed_block *block);

#define BEGIN_TIMED_BLOCK(NAME) struct timed_block _timed_block_##NAME = {};begin_timed_block(&_timed_block_##NAME, #NAME);
#define END_TIMED_BLOCK(NAME) end_timed_block(&_timed_block_##NAME);
