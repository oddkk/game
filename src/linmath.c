#include "linmath.h"
#include <stdlib.h>
#include <string.h>

extern float deg_to_rad(float);
extern float rad_to_deg(float);

#define VECN_DECL(N,S)	  \
	extern void vec##N##S##_set(vec##N##S, const vec##N##S); \
	extern void vec##N##S##_add(vec##N##S, const vec##N##S, const vec##N##S); \
	extern void vec##N##S##_sub(vec##N##S, const vec##N##S, const vec##N##S); \
	extern void vec##N##S##_scale(vec##N##S, const vec##N##S, const vec##S); \
	extern vec##S vec##N##S##_dot(const vec##N##S, const vec##N##S); \
	extern void vec##N##S##_inverse(vec##N##S, const vec##N##S); \
	extern vec##S vec##N##S##_length(const vec##N##S); \
	extern void vec##N##S##_lerp(vec##N##S, const vec##N##S, const vec##N##S, vec##S); \
	extern void vec##N##S##_normalize(vec##N##S, const vec##N##S);

VECN_DECL(2,);
VECN_DECL(3,);
VECN_DECL(4,);
VECN_DECL(2,i);
VECN_DECL(3,i);
VECN_DECL(4,i);

extern void vec3_cross(vec3, const vec3, const vec3);

#define MATN_DECL(N,S)	  \
	extern void mat##N##S##_identity(mat##N##S); \
	extern void mat##N##S##_set(mat##N##S, const mat##N##S); \
	extern void mat##N##S##_transpose(mat##N##S, const mat##N##S);

MATN_DECL(2,);
MATN_DECL(3,);
MATN_DECL(4,);
MATN_DECL(2,i);
MATN_DECL(3,i);
MATN_DECL(4,i);

#define MATN_MULTIPLY_DECL(N,S)	  \
	extern void mat##N##S##_multiply(mat##N##S dest, const mat##N##S lhs, const mat##N##S rhs);

MATN_MULTIPLY_DECL(2,);
MATN_MULTIPLY_DECL(2,i);

MATN_MULTIPLY_DECL(3,);
MATN_MULTIPLY_DECL(3,i);

MATN_MULTIPLY_DECL(4,);
MATN_MULTIPLY_DECL(4,i);

#define MATN_VECN_MULTIPLY_DECL(N,S)	  \
	extern void mat##N##S##_vec##N##S##_multiply(vec##N##S dest, const mat##N##S lhs, const vec##N##S rhs);

MATN_VECN_MULTIPLY_DECL(2,);
MATN_VECN_MULTIPLY_DECL(2,i);

MATN_VECN_MULTIPLY_DECL(3,);
MATN_VECN_MULTIPLY_DECL(3,i);

MATN_VECN_MULTIPLY_DECL(4,);
MATN_VECN_MULTIPLY_DECL(4,i);

extern void mat3_translate(mat3, const vec2);
extern void mat3_translate_into(mat3, const mat3, const vec2);
extern void mat3_rotate(mat3, const float);
extern void mat3_rotate_into(mat3, const mat3, const float);

//extern void mat4_multiply(mat4, const mat4, const mat4);
extern void mat4_inverse(mat4, const mat4);

extern void mat4_frustum(mat4, mat, mat, mat, mat, mat, mat);
extern void mat4_perspective(mat4, mat, mat, mat, mat);
extern void mat4_ortho(mat4, mat, mat, mat, mat, mat, mat);

extern void mat4_translate(mat4, const vec3);
extern void mat4_translate_into(mat4, const mat3, const vec3);
extern void mat4_scale(mat4, const vec3);
extern void mat4_scale_into(mat4, const vec3, const vec3);
extern void mat4_rotate(mat4, const mat, const vec3);
extern void mat4_rotate_into(mat4, const mat4, const mat, const vec3);
extern void mat4_quat_into(mat4, const mat4, const quat);

extern void quat_set(quat, const quat);
extern void quat_calculate_w(quat, const quat);
extern vec quat_dot(const quat, const quat);
extern void quat_inverse(quat, const quat);
extern void quat_conjugate(quat, const quat);
extern vec quat_length(const quat);
extern void quat_normalize(quat, const quat);
extern void quat_multiply(quat, const quat, const quat);
extern void quat_to_mat4(mat4, const quat);
extern void quat_slerp(quat, const quat, const quat, vec);
extern void quat_axis_angles(quat, const vec, const vec3);
extern void quat_euler_angles(quat, const vec3);

extern void aabb_set(aabb, const aabb);
extern void aabb_from_point(aabb, const vec3);
extern void aabb_from_points(aabb, const vec3, const vec3);
extern void aabb_add_point(aabb, const aabb, const vec3);
extern void aabb_add_aabb(aabb, const aabb, const aabb);
extern void aabb_center(vec3, const aabb);
extern void aabb_size(vec3, const aabb);
extern void aabb_extent(vec3, const aabb);
extern void mat4_aabb_multiply(aabb, const mat4, const aabb);

void
vec2_from_string(vec2 dest, const char *str) {
	const char *token_separator = ",";
	char *line = strdup(str);

	char *line_state;

	vec2_set(dest, (vec2){
		atof(strtok_r(line, token_separator, &line_state)),
		atof(strtok_r(NULL, token_separator, &line_state)),
	});

	free(line);
}

void
vec3_from_string(vec3 dest, const char *str) {
	const char *token_separator = ",";
	char *line = strdup(str);

	char *line_state;

	vec3_set(dest, (vec3){
		atof(strtok_r(line, token_separator, &line_state)),
		atof(strtok_r(NULL, token_separator, &line_state)),
		atof(strtok_r(NULL, token_separator, &line_state)),
	});

	free(line);
}

void
vec4_from_string(vec4 dest, const char *str) {
	const char *token_separator = ",";
	char *line = strdup(str);

	char *line_state;

	vec4_set(dest, (vec4){
		atof(strtok_r(line, token_separator, &line_state)),
		atof(strtok_r(NULL, token_separator, &line_state)),
		atof(strtok_r(NULL, token_separator, &line_state)),
		atof(strtok_r(NULL, token_separator, &line_state)),
	});

	free(line);
}

void
quat_from_string(quat dest, const char *str) {
	vec4_from_string(dest, str);
}
