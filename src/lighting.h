#ifndef GAME_LIGHTING_H
#define GAME_LIGHTING_H

#include <stdbool.h>
#include "linmath.h"
#include "shader.h"

#define LIGHT_POINT_MAX 20
#define LIGHT_BOX_MAX 10 

struct light_point {
	vec4 position;

	vec4 diffuse;
	vec4 specular;
};

struct light_box {
	vec4 point1;
	vec4 point2;
	vec4 position;

	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
};

struct lighting_data {
	struct light_point in_light_point[LIGHT_POINT_MAX];
	struct light_box in_light_box[LIGHT_BOX_MAX];
};

struct lighting {
	struct lighting_data data;

	unsigned int uniform_buffer;
};

bool lighting_init(struct lighting *lighting, const shader *shader);
void lighting_update(struct lighting *lighting);
void lighting_free(struct lighting *lighting);

#endif
