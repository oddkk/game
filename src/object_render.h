#include "shader.h"
#include "linmath.h"
#include "model.h"
#include <stdbool.h>
#include <stddef.h>

#define RENDER_QUERY_CAPACITY (256)

struct ui;

struct object_render {
	const struct ui *ui;

	shader *shader;
	struct {
		int modelview;
		int modelview_normal;
		int projection;
		int camera_pos;
		int noise;
		int dissolve;
		int transparancy;
	} uniform;
	mat4 perspective;

	model_shader model_shader_data;

	unsigned int box_vao, box_vbo;
};

struct object_render_entry {
	mat4 modelview;
	float dissolve;
	model *model;
};

struct object_render_queue {
	struct object_render_entry *queue;
	size_t length;
	size_t capacity;
	bool overflow;

	vec3 camera_position;
	quat camera_rotation;
};

struct window;

bool object_render_init(struct object_render *data, shader *shader, const struct ui *ui);
void object_render_free(struct object_render *data);
void object_render_resize(struct object_render *data, vec2i fbsize);
bool object_render_queue_init(struct object_render_queue *q);
void object_render_queue_free(struct object_render_queue *q);
void object_render_queue_push(struct object_render_queue *q, const struct object_render_entry *object);
void object_render_queue_clear(struct object_render_queue *q);
void object_render_queue_render(struct object_render *data, const struct object_render_queue *q, struct window *w);
