#include "model.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include <libgen.h>
#include <GL/glew.h>
#include "array.h"
#include "assets.h"

typedef struct {
	vec3 vertex;
	vec3 uv;
	vec3 normal;
} vertex;

typedef struct {
	int vertex;
	int uv;
	int normal;
} face_vertex;

typedef struct {
	char *name;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float specular_exp;
} material;

DARRAY(vec3);
DARRAY(face_vertex);
DARRAY(vertex);
DARRAY(material);

typedef struct {
	array_face_vertex vertecies;
	int material;
} mesh;

DARRAY(mesh);

static bool model_load_materials(const char *path, array_material *materials) {
	const char *token_separator = " \n\r";
	int current_material = -1;
	size_t line_len = 0;
	char *line = NULL;
	FILE *fp = NULL;

	fp = fopen(path, "r");

	if (!fp) {
		fprintf(stderr, "Could not load material library '%s'\n", path);
		return false;
	}

	while (getline(&line, &line_len, fp) != -1) {
		char *line_state;
		char *token = strtok_r(line, token_separator, &line_state);
		if (!token)
			continue;
		/*  */ if (strcmp(token, "newmtl") == 0) {
			material mtl = {};
			mtl.name = strdup(strtok_r(NULL, token_separator, &line_state));
			current_material = array_material_append(materials, &mtl);
		} else if (strcmp(token, "Ka") == 0) { // ambient
			material *mtl = &materials->data[current_material];
			vec3 c = {
				strtod(strtok_r(NULL, token_separator, &line_state), NULL),
				strtod(strtok_r(NULL, token_separator, &line_state), NULL),
				strtod(strtok_r(NULL, token_separator, &line_state), NULL)
			};
			vec3_set(mtl->ambient, c);
		} else if (strcmp(token, "Kd") == 0) { // diffuse
			material *mtl = &materials->data[current_material];
			vec3 c = {
				strtod(strtok_r(NULL, token_separator, &line_state), NULL),
				strtod(strtok_r(NULL, token_separator, &line_state), NULL),
				strtod(strtok_r(NULL, token_separator, &line_state), NULL)
			};
			vec3_set(mtl->diffuse, c);
		} else if (strcmp(token, "Ks") == 0) { // specular
			material *mtl = &materials->data[current_material];
			vec3 c = {
				strtod(strtok_r(NULL, token_separator, &line_state), NULL),
				strtod(strtok_r(NULL, token_separator, &line_state), NULL),
				strtod(strtok_r(NULL, token_separator, &line_state), NULL)
			};
			vec3_set(mtl->specular, c);
		} else if (strcmp(token, "Ns") == 0) { // specular exp
			material *mtl = &materials->data[current_material];
			mtl->specular_exp = strtod(strtok_r(NULL, token_separator, &line_state), NULL);
		}
	}

	free(line);
	fclose(fp);
	return true;
}

bool model_load_from_file(model *model, const char *asset) {
	const char *token_separator = " \n\r";
	size_t line_len = 0;
	char *line = NULL;
	FILE *fp = NULL;

	array_vec3 vertices = {};
	array_vec3 vertices_uv = {};
	array_vec3 vertices_normals = {};

	array_mesh meshes = {};
	int current_mesh = -1;
	//array_face_vertex faces = {};
	array_material materials = {};
	int current_material = 0;

	unsigned int linum = -1;

	char *path = assets_get_path(asset, NULL);

	fp = fopen(path, "r");

	if (!fp) {
		fprintf(stderr, "Could not open model %s\n", path);
		free(path);
		return false;
	}

	{
		material empty_mtl = {};
		empty_mtl.name = strdup("<unused>");
		current_material = array_material_append(&materials, &empty_mtl);
	}

	while (getline(&line, &line_len, fp) != -1) {
		char *line_state;
		char *token = strtok_r(line, token_separator, &line_state);
		++linum;
		if (!token)
			continue;
		/*  */ if (strcmp(token, "v") == 0) {
			vec3 v = {
				strtod(strtok_r(NULL, token_separator, &line_state), NULL),
				strtod(strtok_r(NULL, token_separator, &line_state), NULL),
				strtod(strtok_r(NULL, token_separator, &line_state), NULL)
			};
			array_vec3_append(&vertices, &v);
		} else if (strcmp(token, "vt") == 0) {
			vec3 v = {
				strtod(strtok_r(NULL, token_separator, &line_state), NULL),
				strtod(strtok_r(NULL, token_separator, &line_state), NULL),
				0
			};
			array_vec3_append(&vertices_uv, &v);
		} else if (strcmp(token, "vn") == 0) {
			vec3 v = {
				strtod(strtok_r(NULL, token_separator, &line_state), NULL),
				strtod(strtok_r(NULL, token_separator, &line_state), NULL),
				strtod(strtok_r(NULL, token_separator, &line_state), NULL)
			};
			array_vec3_append(&vertices_normals, &v);
		} else if (strcmp(token, "f") == 0) {
			while ((token = strtok_r(NULL, token_separator, &line_state)) != NULL) {
				char *subtoken;
				face_vertex f = {};
				if ((subtoken = strsep(&token, "/")) != NULL) {
					f.vertex = strtol(subtoken, NULL, 10);
				}
				else {
					continue;
				}
				if ((subtoken = strsep(&token, "/")) != NULL &&
				    strlen(subtoken) > 0) {
					f.uv = strtol(subtoken, NULL, 10);
				}
				if ((subtoken = strsep(&token, "/")) != NULL &&
				    strlen(subtoken) > 0) {
					f.normal = strtol(subtoken, NULL, 10);
				}

				if (current_mesh < 0) {
					fprintf(stderr, "%s:%i: missing material before faces\n", path, linum);
					continue;
				}
					
				array_face_vertex_append(&meshes.data[current_mesh].vertecies, &f);
			}
		} else if (strcmp(token, "mtllib") == 0) {
			char *libasset = strtok_r(NULL, token_separator, &line_state);
			char *libpath = assets_get_path(libasset, path);

			model_load_materials(libpath, &materials);

			free(libpath);
		} else if (strcmp(token, "usemtl") == 0) {
			char *mtl_name = strtok_r(NULL, token_separator, &line_state);
			for (size_t i = 0; i < materials.size; ++i) {
				if (strcmp(materials.data[i].name, mtl_name) == 0) {
					current_material = i;
				}
			}

			bool found_existing_mesh = false;
			for (size_t i = 0; i < meshes.size; ++i) {
				if (meshes.data[i].material == current_material) {
					current_mesh = i;
					found_existing_mesh = true;
					break;
				}
			}

			if (!found_existing_mesh) {
				mesh m = {
					.material = current_material,
					.vertecies = {},
				};
				current_mesh = array_mesh_append(&meshes, &m);
			}
		}
	}

	free(line);
	fclose(fp);

	array_vertex compvertices = {};

	if (model->meshes != NULL) {
		free(model->meshes);
		model->loaded = false;
		model->meshes = NULL;
		model->num_meshes = 0;
	}

	model->meshes = calloc(meshes.size, sizeof(struct model_mesh));
	model->num_meshes = meshes.size;
	bool model_aabb_set = false;

	for (size_t i = 0; i < meshes.size; ++i) {
		mesh *m = &meshes.data[i];
		struct model_mesh *res = &model->meshes[i];
		material *mtl = &materials.data[m->material];
		bool mesh_aabb_set = false;

		assert((size_t)m->material < materials.size);

		vec3_set(res->ambient, mtl->ambient);
		vec3_set(res->diffuse, mtl->diffuse);
		vec3_set(res->specular, mtl->specular);
		res->specular_exp = mtl->specular_exp;

		array_vertex_clear(&compvertices);

		for (size_t j = 0; j < m->vertecies.size; ++j) {
			face_vertex *f = &m->vertecies.data[j];
			vertex v = {};
			if (f->vertex != 0)
				vec3_set(v.vertex, vertices.data[f->vertex - 1]);
			if (f->uv != 0)
				vec3_set(v.uv, vertices_uv.data[f->uv - 1]);
			if (f->normal != 0)
				vec3_set(v.normal, vertices_normals.data[f->normal - 1]);
			array_vertex_append(&compvertices, &v);
			if (!mesh_aabb_set) {
				aabb_from_point(res->bounding_box, v.vertex);
				mesh_aabb_set = true;
			} else {
				aabb_add_point(res->bounding_box, res->bounding_box, v.vertex);
			}
		}

		if (!model_aabb_set) {
			model_aabb_set = true;
			aabb_set(model->bounding_box, res->bounding_box);
		} else {
			aabb_add_aabb(model->bounding_box, model->bounding_box, res->bounding_box);
		}
		res->num_vertecies = compvertices.size;

		glGenVertexArrays(1, &res->vao);
		glBindVertexArray(res->vao);

		glGenBuffers(1, &res->vbo);
		glBindBuffer(GL_ARRAY_BUFFER, res->vbo);

		glBufferData(GL_ARRAY_BUFFER,
					 compvertices.size * sizeof(vertex),
					 compvertices.data,
					 GL_STATIC_DRAW);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), (void*)0);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), (void*)sizeof(vec3));
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), (void*)(sizeof(vec3) * 2));

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
	}

	model->loaded = true;

	free(compvertices.data);

	free(vertices.data);
	free(vertices_uv.data);
	free(vertices_normals.data);

	for (size_t i = 0; i < meshes.size; ++i) {
		free(meshes.data[i].vertecies.data);
	}
	free(meshes.data);
	for (size_t i = 0; i < materials.size; ++i) {
		free(materials.data[i].name);
	}
	free(materials.data);

	free(path);

	return true;
}

void model_unload(model *mdl) {
	mdl->loaded = false;
	for (size_t i = 0; i < mdl->num_meshes; ++i) {
		struct model_mesh *mesh = &mdl->meshes[i];
		glDeleteVertexArrays(1, &mesh->vao);
		glDeleteBuffers(1, &mesh->vbo);
	}
	free(mdl->meshes);
	mdl->meshes = NULL;
	mdl->num_meshes = 0;
}

void model_draw(const model *mdl, const model_shader *shdr) {
	assert(mdl->loaded);
	for (unsigned int i = 0; i < mdl->num_meshes; ++i) {
		struct model_mesh *msh = &mdl->meshes[i];
		glUniform3fv(shdr->in_ambient, 1, msh->ambient);
		glUniform3fv(shdr->in_diffuse, 1, msh->diffuse);
		glUniform3fv(shdr->in_specular, 1, msh->specular);
		glUniform1f(shdr->in_specular_exp, msh->specular_exp);

		glBindVertexArray(msh->vao);
		glDrawArrays(GL_TRIANGLES, 0, msh->num_vertecies);
	}
}

model_shader model_make_model_shader(unsigned int program) {
	model_shader shader = {
		.in_ambient = glGetUniformLocation(program, "in_ambient"),
		.in_diffuse = glGetUniformLocation(program, "in_diffuse"),
		.in_specular = glGetUniformLocation(program, "in_specular"),
		.in_specular_exp = glGetUniformLocation(program, "in_specular_exp"),
	};
	return shader;
}
