#include "lighting.h"
#include <string.h>
#include <GL/glew.h>

bool
lighting_init(struct lighting *lighting, const shader *shader) {
	GLuint shader_data_id = glGetUniformBlockIndex(shader->program, "ShaderDataBlock");

	if (shader_data_id == GL_INVALID_INDEX) {
		return false;
	}
	glUniformBlockBinding(shader->program, shader_data_id, 0);

	memset(&lighting->data, 0, sizeof(struct lighting_data));

	glGenBuffers(1, &lighting->uniform_buffer);
	glBindBuffer(GL_UNIFORM_BUFFER, lighting->uniform_buffer);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(struct lighting_data), &lighting->data, GL_DYNAMIC_DRAW);
	glBindBufferBase(GL_UNIFORM_BUFFER, shader_data_id, lighting->uniform_buffer);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	return true;
}

void
lighting_update(struct lighting *lighting) {
	glBindBuffer(GL_UNIFORM_BUFFER, lighting->uniform_buffer);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(struct lighting_data), &lighting->data);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void
lighting_free(struct lighting *lighting) {
	glDeleteBuffers(1, &lighting->uniform_buffer);
	lighting->uniform_buffer = 0;
}
