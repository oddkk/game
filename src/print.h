#ifndef GAME_PRINT_H
#define GAME_PRINT_H

#include <stdarg.h>
#include <stdint.h>

void vprintf_cb(void(*cb)(uint32_t, void*), void *data, char *frmt, va_list argp);

#endif
