#ifndef GAME_WINDOW_H
#define GAME_WINDOW_H

#include <stdbool.h>

typedef enum {
	WINDOW_MODE_FULLSCREEN = (1 << 0),
	WINDOW_MODE_WINDOWED   = (1 << 1),
} window_mode;

struct window {
	window_mode mode;
	int width, height;
	int physical_width,
		physical_height;

	float dpi;

	void *handle;
};

bool window_init();
void window_terminate();
bool window_create(struct window *window);
void window_free(struct window *window);
bool window_shouldclose(struct window *window);
void window_swapbuffer(struct window *window);

#endif
