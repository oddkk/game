#include "assets.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>

char *assets_get_path(const char *asset, const char *owner) {
	char *o = NULL;
	bool found = false;
	size_t num_prefixes = 2;
	const char *prefixes[] = {
		NULL, //placeholder for the owners directory
		ASSETS_BASE_DIR,
	};

	if (owner != NULL && asset[0] != '/') {
		o = strdup(owner);
		prefixes[0] = dirname(o);
	}

	char *path = NULL;

	for (size_t i = 0; i < num_prefixes; ++i) {
		const char *dir = prefixes[i];
		if (dir == NULL)
			continue;
		size_t path_len = (strlen(dir) +
						   strlen(asset) +
						   /* for the '/'*/ 1 +
		                   /* for the \0 */ 1);
		char *new_path = realloc(path, path_len * sizeof(char));
		if (new_path) {
			path = new_path;
			memset(path, 0, path_len * sizeof(char));

			strcat(path, dir);
			strcat(path, "/");
			strcat(path, asset);

			//fprintf(stderr, "trying %s\n", path);

			if (access(path, F_OK) != -1) {
				found = true;
				break;
			}
		}
	}

	free(o);

	if (!found) {
		free(path);
		path = NULL;
	}
	return path;
}
