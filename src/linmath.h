#ifndef GAME_LINMATH_H
#define GAME_LINMATH_H

#define PI (3.14159265358979323846)

typedef float vec;
typedef int veci;
typedef float mat;
typedef int mati;

double sqrt(double);
double fabs(double);
double sin(double);
double cos(double);
double acos(double);
double tan(double);
double fmin(double, double);
double fmax(double, double);

inline float
deg_to_rad(float deg) {
	return deg * PI / 180.0;
}

inline float
rad_to_deg(float rad) {
	return rad * 180.0 / PI;
}

#define VECN_SET(N, S)	  \
	inline void \
	vec##N##S##_set(vec##N##S dest, const vec##N##S lhs) { \
		for (int i = 0; i < N; ++i) \
			dest[i] = lhs[i]; \
	}

#define VECN_ADD(N,S)	  \
	inline void \
	vec##N##S##_add(vec##N##S dest, const vec##N##S lhs, const vec##N##S rhs) { \
		for (int i = 0; i < N; ++i) \
			dest[i] = lhs[i] + rhs[i]; \
	}

#define VECN_SUB(N,S)	  \
	inline void \
	vec##N##S##_sub(vec##N##S dest, const vec##N##S lhs, const vec##N##S rhs) { \
		for (int i = 0; i < N; ++i) \
			dest[i] = lhs[i] - rhs[i]; \
	}

#define VECN_SCALE(N,S)	  \
	inline void \
	vec##N##S##_scale(vec##N##S dest, const vec##N##S lhs, vec##S rhs) { \
		for (int i = 0; i < N; ++i) \
			dest[i] = lhs[i] * rhs; \
	}

#define VECN_DOT(N,S)	  \
	inline vec##S \
	vec##N##S##_dot(const vec##N##S lhs, const vec##N##S rhs) { \
		vec##S out = 0; \
		for (int i = 0; i < N; ++i) \
			out += lhs[i] * rhs[i]; \
		return out; \
	}

#define VECN_INVERSE(N,S)	  \
	inline void \
	vec##N##S##_inverse(vec##N##S dest, const vec##N##S lhs) { \
		for (int i = 0; i < N; ++i) \
			dest[i] = -lhs[i]; \
	}

#define VECN_LENGTH(N,S)	  \
	inline vec##S \
	vec##N##S##_length(const vec##N##S lhs) { \
		vec##S sum = 0; \
		for (int i = 0; i < N; ++i) \
			sum += lhs[i] * lhs[i]; \
		return sqrt(sum); \
	}

#define VECN_LERP(N,S)	  \
	inline void \
	vec##N##S##_lerp(vec##N##S dest, const vec##N##S lhs, const vec##N##S rhs, vec##S lerp) { \
		for (int i = 0; i < N; ++i) \
			dest[i] = lhs[i] + lerp * (rhs[i] - lhs[i]); \
	}

#define VECN_NORMALIZE(N,S)	  \
	inline void \
	vec##N##S##_normalize(vec##N##S dest, const vec##N##S lhs) { \
		vec##S len = vec##N##S##_length(lhs); \
		vec##N##S##_scale(dest, lhs, 1.0 / len); \
	}

#define VECN(N, S)	  \
	typedef vec##S vec##N##S[N]; \
	VECN_SET(N,S) \
	VECN_ADD(N,S) \
	VECN_SUB(N,S) \
	VECN_SCALE(N,S) \
	VECN_DOT(N,S) \
	VECN_INVERSE(N,S) \
	VECN_LENGTH(N,S) \
	VECN_NORMALIZE(N,S)

VECN(2,);
VECN(3,);
VECN(4,);

VECN(2,i);
VECN(3,i);
VECN(4,i);

inline void
vec3_cross(vec3 dest, const vec3 lhs, const vec3 rhs) {
	vec x1 = lhs[0], y1 = lhs[1], z1 = lhs[2],
		x2 = rhs[0], y2 = rhs[1], z2 = rhs[2];

	dest[0] = y1 * z2 - z1 * y2;
	dest[1] = z1 * x2 - x1 * z2;
	dest[2] = x1 * y2 - y1 * x2;
}

void
vec2_from_string(vec2 dest, const char *str);

void
vec3_from_string(vec3 dest, const char *str);

void
vec4_from_string(vec4 dest, const char *str);

#define MATN_IDENTITY(N,S)	  \
	inline void \
	mat##N##S##_identity(mat##N##S lhs) { \
		for (int i = 0; i < N*N; ++i) \
			lhs[i] = ((i / N) == (i % N)) ? (mat)1.0 : (mat)0.0; \
	}

#define MATN_SET(N,S)	  \
	inline void \
	mat##N##S##_set(mat##N##S dest, const mat##N##S lhs) { \
		for (int i = 0; i < N*N; ++i) \
			dest[i] = lhs[i]; \
	}

#define MATN_TRANSPOSE(N,S)	  \
	inline void \
	mat##N##S##_transpose(mat##N##S dest, const mat##N##S lhs) { \
		for (int i = 0; i < N - 1; ++i) { \
			for (int j = i + 1; j < N; ++j) { \
				mat##S tmp = lhs[i*N+j]; \
				dest[i*N+j] = lhs[j*N+i]; \
				dest[j*N+i] = tmp; \
			} \
		} \
	}

#define MATN(N,S)	  \
	typedef mat##S mat##N##S[N*N]; \
	MATN_IDENTITY(N,S); \
	MATN_SET(N,S); \
	MATN_TRANSPOSE(N,S);

MATN(2,);
MATN(3,);
MATN(4,);

MATN(2,i);
MATN(3,i);
MATN(4,i);

#define MAT2_MULTIPLY(S)	  \
	inline void \
	mat2##S##_multiply(mat2##S dest, const mat2##S lhs, const mat2##S rhs) { \
		mat##S a00 = lhs[0], a01 = lhs[1], \
			a10 = lhs[2], a11 = lhs[3], \
			b00 = rhs[0], b01 = rhs[1], \
			b10 = rhs[2], b11 = rhs[3]; \
	  \
		dest[0] = b00 * a00 + b01 * a10; \
		dest[1] = b00 * a01 + b01 * a11; \
		dest[2] = b10 * a00 + b11 * a10; \
		dest[3] = b10 * a01 + b11 * a10; \
	}

MAT2_MULTIPLY();
MAT2_MULTIPLY(i);

#define MAT2_VEC2_MULTIPLY(S)	  \
	inline void \
	mat2##S##_vec2##S##_multiply(vec2##S dest, const mat2##S lhs, const vec2##S rhs) { \
		mat##S a00 = lhs[0], a01 = lhs[1], \
			a10 = lhs[2], a11 = lhs[3], \
			b0 = rhs[0], b1 = rhs[1]; \
	  \
		dest[0] = a00 * b0 + a10 * b1; \
		dest[1] = a01 * b0 + a11 * b1; \
	}

MAT2_VEC2_MULTIPLY();
MAT2_VEC2_MULTIPLY(i);

#define MAT3_MULTIPLY(S)	  \
	inline void \
	mat3##S##_multiply(mat3##S dest, const mat3##S lhs, const mat3##S rhs) { \
		mat##S \
			a00 = lhs[0], a01 = lhs[1], a02 = lhs[2], \
			a10 = lhs[3], a11 = lhs[4], a12 = lhs[5], \
			a20 = lhs[6], a21 = lhs[7], a22 = lhs[8], \
	  \
			b00 = rhs[0], b01 = rhs[1], b02 = rhs[2], \
			b10 = rhs[3], b11 = rhs[4], b12 = rhs[5], \
			b20 = rhs[6], b21 = rhs[7], b22 = rhs[8]; \
	  \
		dest[0] = b00 * a00 + b01 * a10 + b02 * a20; \
		dest[1] = b00 * a01 + b01 * a11 + b02 * a21; \
		dest[2] = b00 * a02 + b01 * a12 + b02 * a22; \
		dest[3] = b10 * a00 + b11 * a10 + b12 * a20; \
		dest[4] = b10 * a01 + b11 * a11 + b12 * a21; \
		dest[5] = b10 * a02 + b11 * a12 + b12 * a22; \
		dest[6] = b20 * a00 + b21 * a10 + b22 * a20; \
		dest[7] = b20 * a01 + b21 * a11 + b22 * a21; \
		dest[8] = b20 * a02 + b21 * a12 + b22 * a22; \
	}

MAT3_MULTIPLY();
MAT3_MULTIPLY(i);

#define MAT3_VEC3_MULTIPLY(S)	  \
	inline void \
	mat3##S##_vec3##S##_multiply(vec3##S dest, const mat3##S lhs, const vec3##S rhs) { \
		mat##S \
			a00 = lhs[0], a01 = lhs[1], a02 = lhs[2], \
			a10 = lhs[3], a11 = lhs[4], a12 = lhs[5], \
			a20 = lhs[6], a21 = lhs[7], a22 = lhs[8], \
\
			b0 = rhs[0], b1 = rhs[1], b2 = rhs[2]; \
	  \
		dest[0] = a00 * b0 + a10 * b1 + a20 * b2; \
		dest[1] = a01 * b0 + a11 * b1 + a21 * b2; \
		dest[2] = a02 * b0 + a12 * b1 + a22 * b2; \
	}

MAT3_VEC3_MULTIPLY();
MAT3_VEC3_MULTIPLY(i);

inline void
mat3_translate(mat3 dest, const vec2 lhs) {
	mat3_identity(dest);
	dest[6] = lhs[0];
	dest[7] = lhs[1];
}

inline void
mat3_translate_into(mat3 dest, const mat3 lhs, const vec2 rhs) {
	mat3 m;
	mat3_translate(m, rhs);
	mat3_multiply(dest, lhs, m);
}

inline void
mat3_rotate(mat3 dest, const float lhs) {
	float c = cos(lhs), s = sin(lhs);
	dest[0] = c;
	dest[1] = -s;
	dest[2] = 0;
	dest[3] = s;
	dest[4] = c;
	dest[5] = 0;
	dest[6] = 0;
	dest[7] = 0;
	dest[8] = 1;
}

inline void
mat3_rotate_into(mat3 dest, const mat3 lhs, const float rhs) {
	mat3 m;
	mat3_rotate(m, rhs);
	mat3_multiply(dest, lhs, m);
}

#define MAT4_MULTIPLY(S)	  \
	inline void \
	mat4##S##_multiply(mat4##S dest, const mat4##S lhs, const mat4##S rhs) { \
		mat##S \
			a00 = lhs[0],  a01 = lhs[1],  a02 = lhs[2],  a03 = lhs[3], \
			a10 = lhs[4],  a11 = lhs[5],  a12 = lhs[6],  a13 = lhs[7], \
			a20 = lhs[8],  a21 = lhs[9],  a22 = lhs[10], a23 = lhs[11], \
			a30 = lhs[12], a31 = lhs[13], a32 = lhs[14], a33 = lhs[15], \
	  \
			b00 = rhs[0],  b01 = rhs[1],  b02 = rhs[2],  b03 = rhs[3], \
			b10 = rhs[4],  b11 = rhs[5],  b12 = rhs[6],  b13 = rhs[7], \
			b20 = rhs[8],  b21 = rhs[9],  b22 = rhs[10], b23 = rhs[11], \
			b30 = rhs[12], b31 = rhs[13], b32 = rhs[14], b33 = rhs[15]; \
	  \
		dest[0]  = b00 * a00 + b01 * a10 + b02 * a20 + b03 * a30; \
		dest[1]  = b00 * a01 + b01 * a11 + b02 * a21 + b03 * a31; \
		dest[2]  = b00 * a02 + b01 * a12 + b02 * a22 + b03 * a32; \
		dest[3]  = b00 * a03 + b01 * a13 + b02 * a23 + b03 * a33; \
		dest[4]  = b10 * a00 + b11 * a10 + b12 * a20 + b13 * a30; \
		dest[5]  = b10 * a01 + b11 * a11 + b12 * a21 + b13 * a31; \
		dest[6]  = b10 * a02 + b11 * a12 + b12 * a22 + b13 * a32; \
		dest[7]  = b10 * a03 + b11 * a13 + b12 * a23 + b13 * a33; \
		dest[8]  = b20 * a00 + b21 * a10 + b22 * a20 + b23 * a30; \
		dest[9]  = b20 * a01 + b21 * a11 + b22 * a21 + b23 * a31; \
		dest[10] = b20 * a02 + b21 * a12 + b22 * a22 + b23 * a32; \
		dest[11] = b20 * a03 + b21 * a13 + b22 * a23 + b23 * a33; \
		dest[12] = b30 * a00 + b31 * a10 + b32 * a20 + b33 * a30; \
		dest[13] = b30 * a01 + b31 * a11 + b32 * a21 + b33 * a31; \
		dest[14] = b30 * a02 + b31 * a12 + b32 * a22 + b33 * a32; \
		dest[15] = b30 * a03 + b31 * a13 + b32 * a23 + b33 * a33; \
	}

MAT4_MULTIPLY();
MAT4_MULTIPLY(i);

#define MAT4_VEC4_MULTIPLY(S)	  \
	inline void \
	mat4##S##_vec4##S##_multiply(vec4##S dest, const mat4##S lhs, const vec4##S rhs) { \
		mat##S \
			a00 = lhs[0],  a01 = lhs[1],  a02 = lhs[2],  a03 = lhs[3], \
			a10 = lhs[4],  a11 = lhs[5],  a12 = lhs[6],  a13 = lhs[7], \
			a20 = lhs[8],  a21 = lhs[9],  a22 = lhs[10], a23 = lhs[11], \
			a30 = lhs[12], a31 = lhs[13], a32 = lhs[14], a33 = lhs[15], \
\
			b0 = rhs[0], b1 = rhs[1], b2 = rhs[2], b3 = rhs[3]; \
	  \
		dest[0] = a00 * b0 + a10 * b1 + a20 * b2 + a30 * b3; \
		dest[1] = a01 * b0 + a11 * b1 + a21 * b2 + a31 * b3; \
		dest[2] = a02 * b0 + a12 * b1 + a22 * b2 + a32 * b3; \
		dest[3] = a03 * b0 + a13 * b1 + a23 * b2 + a33 * b3; \
	}

MAT4_VEC4_MULTIPLY();
MAT4_VEC4_MULTIPLY(i);

inline void
mat4_inverse(mat4 dest, const mat4 lhs) {
	mat a00 = lhs[0],  a01 = lhs[1],  a02 = lhs[2],  a03 = lhs[3],
		a10 = lhs[4],  a11 = lhs[5],  a12 = lhs[6],  a13 = lhs[7],
		a20 = lhs[8],  a21 = lhs[9],  a22 = lhs[10], a23 = lhs[11],
		a30 = lhs[12], a31 = lhs[13], a32 = lhs[14], a33 = lhs[15];

	mat b00 = a00 * a11 - a01 * a10,
		b01 = a00 * a12 - a02 * a10,
		b02 = a00 * a13 - a03 * a10,
		b03 = a01 * a12 - a02 * a11,
		b04 = a01 * a13 - a03 * a11,
		b05 = a02 * a13 - a03 * a12,
		b06 = a20 * a31 - a21 * a30,
		b07 = a20 * a32 - a22 * a30,
		b08 = a20 * a33 - a23 * a30,
		b09 = a21 * a32 - a22 * a31,
		b10 = a21 * a33 - a23 * a31,
		b11 = a22 * a33 - a23 * a32;

	mat d = (b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06),
		invDet;

	// Calculate the determinant
	if (!d) { return; }
	invDet = 1 / d;

	dest[0] = (a11 * b11 - a12 * b10 + a13 * b09) * invDet;
	dest[1] = (-a01 * b11 + a02 * b10 - a03 * b09) * invDet;
	dest[2] = (a31 * b05 - a32 * b04 + a33 * b03) * invDet;
	dest[3] = (-a21 * b05 + a22 * b04 - a23 * b03) * invDet;
	dest[4] = (-a10 * b11 + a12 * b08 - a13 * b07) * invDet;
	dest[5] = (a00 * b11 - a02 * b08 + a03 * b07) * invDet;
	dest[6] = (-a30 * b05 + a32 * b02 - a33 * b01) * invDet;
	dest[7] = (a20 * b05 - a22 * b02 + a23 * b01) * invDet;
	dest[8] = (a10 * b10 - a11 * b08 + a13 * b06) * invDet;
	dest[9] = (-a00 * b10 + a01 * b08 - a03 * b06) * invDet;
	dest[10] = (a30 * b04 - a31 * b02 + a33 * b00) * invDet;
	dest[11] = (-a20 * b04 + a21 * b02 - a23 * b00) * invDet;
	dest[12] = (-a10 * b09 + a11 * b07 - a12 * b06) * invDet;
	dest[13] = (a00 * b09 - a01 * b07 + a02 * b06) * invDet;
	dest[14] = (-a30 * b03 + a31 * b01 - a32 * b00) * invDet;
	dest[15] = (a20 * b03 - a21 * b01 + a22 * b00) * invDet;
}

inline void
mat4_frustum(mat4 dest, mat left, mat right, mat bottom, mat top, mat near, mat far) {
	mat rl = (right - left),
		tb = (top - bottom),
		fn = (far - near);

	mat4_set(dest, (mat4){
			(near * 2) / rl, 0, 0, 0,
				0, (near * 2) / tb, 0, 0,
				(right + left) / rl, (top + bottom) / tb, -(far + near) / fn, -1,
				0, 0, -(far * near * 2) / fn, 0
				});
}

inline void
mat4_perspective(mat4 dest, mat fovy, mat aspect, mat near, mat far) {
	mat top = near * tan(fovy * PI / 360.0),
		right = top * aspect;
	mat4_frustum(dest, -right, right, -top, top, near, far);
}

inline void
mat4_ortho(mat4 dest, mat left, mat right, mat bottom, mat top, mat near, mat far) {
	mat rl = (right - left),
		tb = (top - bottom),
		fn = (far - near);
	mat4_set(dest, (mat4){
			2 / rl, 0, 0, 0,
				0, 2 / tb, 0, 0,
				0, 0, -2 / fn, 0,
				-(left + right) / rl, -(top + bottom) / tb, -(far + near) / fn, 1
				});
}

inline void
mat4_translate(mat4 dest, const vec3 vec) {
	mat4_set(dest, (mat4){
			1,       0,       0,       0, 
				0,       1,       0,       0, 
				0,       0,       1,       0, 
				vec[0], vec[1],  vec[2],   1
				});
}

inline void
mat4_translate_into(mat4 dest, const mat4 lhs, const vec3 vec) {
	mat4 m;
	mat4_translate(m, vec);
	mat4_multiply(dest, lhs, m);
}

inline void
mat4_scale(mat4 dest, const vec3 vec) {
	mat4_set(dest, (mat4){
			vec[0],  0,       0,       0,
				0,       vec[1],  0,       0,
				0,       0,       vec[2],  0,
				0,       0,       0,       1
				});
}

inline void
mat4_scale_into(mat4 dest, const mat4 lhs, const vec3 vec) {
	mat4 m;
	mat4_scale(m, vec);
	mat4_multiply(dest, lhs, m);
}

inline void
mat4_rotate(mat4 dest, const mat angle, const vec3 a) {
	mat s = sin(angle),
		c = cos(angle);
	mat x = a[0], y = a[1], z = a[2];
	mat4_set(dest, (mat4){
			x + (y + z) * c, z * (-s), y * s, 0,
				z * s, y + (x + z) * c, x * (-s), 0,
				y * (-s), x * s, z + (x + y) * c, 0,
				0, 0, 0, 1
				});
}

inline void
mat4_rotate_into(mat4 dest, const mat4 lhs, const mat angle, const vec3 a) {
	mat4 m;
	mat4_rotate(m, angle, a);
	mat4_multiply(dest, lhs, m);
}

typedef vec4 quat;

inline void
quat_set(quat dest, const quat lhs) {
	vec4_set(dest, lhs);
}

inline void
quat_calculate_w(quat dest, const quat lhs) {
	double x = lhs[0], y = lhs[1], z = lhs[2];
	quat_set(dest, (quat) {
			x, y, z, -sqrt(fabs(1.0 - x*x - y*y - z*z))
				});
}

inline vec
quat_dot(const quat lhs, const quat rhs) {
	return vec4_dot(lhs, rhs);
}

inline void
quat_inverse(quat dest, const quat lhs) {
	vec dot = quat_dot(lhs, lhs),
		inv_dot = 1.0/dot;
	quat_set(dest, (quat) {
			-lhs[0] * inv_dot,
				-lhs[1] * inv_dot,
				-lhs[2] * inv_dot,
				lhs[3] * inv_dot
				});
}

inline void
quat_conjugate(quat dest, const quat lhs) {
	quat_set(dest, (quat) {
			-lhs[0],
				-lhs[1],
				-lhs[2],
				lhs[3]
				});
}

inline vec
quat_length(const quat lhs) {
	vec x = lhs[0], y = lhs[1], z = lhs[2], w = lhs[3];
	return sqrt(x*x + y*y + z*z + w*w);
}

inline void
quat_normalize(quat dest, const quat lhs) {
	vec x = lhs[0], y = lhs[1], z = lhs[2], w = lhs[3],
		length = quat_length(lhs),
		inv_length = 1.0 / length;
	if (length == 0) {
		quat_set(dest, (quat) { 0, 0, 0, 0 });
	}
	else {
		quat_set(dest, (quat) {
				x * inv_length,
					y * inv_length,
					z * inv_length,
					w * inv_length
					});
	}
}

inline void
quat_multiply(quat dest, const quat lhs, const quat rhs) {
	vec qax = lhs[0], qay = lhs[1], qaz = lhs[2], qaw = lhs[3],
		qbx = rhs[0], qby = rhs[1], qbz = rhs[2], qbw = rhs[3];

	quat_set(dest, (quat) {
			qax * qbw + qaw * qbx + qay * qbz - qaz * qby,
				qay * qbw + qaw * qby + qaz * qbx - qax * qbz,
				qaz * qbw + qaw * qbz + qax * qby - qay * qbx,
				qaw * qbw - qax * qbx - qay * qby - qaz * qbz
				});
}

inline void
quat_to_mat4(mat4 dest, const quat lhs) {
	mat x = lhs[0], y = lhs[1], z = lhs[2], w = lhs[3],
		x2 = x + x,
		y2 = y + y,
		z2 = z + z,

		xx = x * x2,
		xy = x * y2,
		xz = x * z2,
		yy = y * y2,
		yz = y * z2,
		zz = z * z2,
		wx = w * x2,
		wy = w * y2,
		wz = w * z2;

	mat4_set(dest, (mat4) {
			1 - (yy + zz), xy + wz, xz - wy, 0,
				xy - wz, 1 - (xx + zz), yz + wx, 0,
				xz + wy, yz - wx, 1 - (xx + yy), 0,
				0, 0, 0, 1
				});
}

inline void
quat_slerp(quat dest, const quat lhs, const quat rhs, vec slerp) {
	vec cosHalfTheta = lhs[0] * rhs[0] + lhs[1] * rhs[1] + lhs[2] * rhs[2] + lhs[3] * rhs[3],
		halfTheta,
		sinHalfTheta,
		ratioA,
		ratioB;

	if (fabs(cosHalfTheta) >= 1.0) {
		quat_set(dest, (quat) {
				lhs[0],
					lhs[1],
					lhs[2],
					lhs[3]
					});
		return;
	}

	halfTheta = acos(cosHalfTheta);
	sinHalfTheta = sqrt(1.0 - cosHalfTheta * cosHalfTheta);

	if (fabs(sinHalfTheta) < 0.001) {

		quat_set(dest, (quat) {
				(lhs[0] * 0.5 + rhs[0] * 0.5),
					(lhs[1] * 0.5 + rhs[1] * 0.5),
					(lhs[2] * 0.5 + rhs[2] * 0.5),
					(lhs[3] * 0.5 + rhs[3] * 0.5)
					});
		return;
	}

	ratioA = sin((1 - slerp) * halfTheta) / sinHalfTheta;
	ratioB = sin(slerp * halfTheta) / sinHalfTheta;

	quat_set(dest, (quat) {
			(lhs[0] * ratioA + rhs[0] * ratioB),
				(lhs[1] * ratioA + rhs[1] * ratioB),
				(lhs[2] * ratioA + rhs[2] * ratioB),
				(lhs[3] * ratioA + rhs[3] * ratioB)
				});
}

inline void
quat_axis_angles(quat dest, const vec angle, const vec3 axis) {
	vec r = angle / 2,
		s = sin(r);
	quat_set(dest, (quat) {
			s * axis[0],
				s * axis[1],
				s * axis[2],
				cos(r)
				});
}

inline void
quat_euler_angles(quat dest, const vec3 angles) {
	quat q;
	quat_axis_angles(dest, angles[0], (vec3){1,0,0});
	quat_axis_angles(q, angles[1], (vec3){0,1,0});
	quat_multiply(dest, dest, q);
	quat_axis_angles(q, angles[2], (vec3){0,0,1});
	quat_multiply(dest, dest, q);
}

inline void
mat4_quat_into(mat4 dest, const mat4 lhs, const quat rhs) {
	mat4 m;
	quat_to_mat4(m, rhs);
	mat4_multiply(dest, lhs, m);
}

void
quat_from_string(quat dest, const char *str);

// Axis-Aligned Bounding Box
// 2x vec3 (min, max)
typedef vec aabb[3*2];

inline void
aabb_set(aabb dest, const aabb lhs) {
	for (int i = 0; i < 3*2; ++i) {
		dest[i] = lhs[i];
	}
}

inline void
aabb_from_point(aabb dest, const vec3 lhs) {
	for (int i = 0; i < 3; ++i) {
		dest[0+i] = lhs[i];
		dest[3+i] = lhs[i];
	}
}

inline void
aabb_from_points(aabb dest, const vec3 lhs, const vec3 rhs) {
	for (int i = 0; i < 3; ++i) {
		float a = lhs[i], b = rhs[i];
		dest[0+i] = fmin(a, b);
		dest[3+i] = fmax(a, b);
	}
}

inline void
aabb_add_point(aabb dest, const aabb lhs, const vec3 rhs) {
	for (int i = 0; i < 3; ++i) {
		dest[0+i] = fmin(lhs[0+i], rhs[i]);
		dest[3+i] = fmax(lhs[3+i], rhs[i]);
	}
}

inline void
aabb_add_aabb(aabb dest, const aabb lhs, const aabb rhs) {
	for (int i = 0; i < 3; ++i) {
		dest[0+i] = fmin(lhs[0+i], rhs[0+i]);
		dest[3+i] = fmax(lhs[3+i], rhs[3+i]);
	}
}

inline void
aabb_center(vec3 dest, const aabb lhs) {
	for (int i = 0; i < 3; ++i) {
		dest[i] = lhs[0+i] + (lhs[3+i] - lhs[0+i]) / 2;
	}
}

inline void
aabb_size(vec3 dest, const aabb lhs) {
	for (int i = 0; i < 3; ++i) {
		dest[i] = lhs[3+i] - lhs[0+i];
	}
}

inline void
aabb_extent(vec3 dest, const aabb lhs) {
	for (int i = 0; i < 3; ++i) {
		dest[i] = (lhs[3+i] - lhs[0+i]) / 2.0;
	}
}

inline void
mat4_aabb_multiply(aabb dest, const mat4 lhs, const aabb rhs) {
	vec4 center, extent;
	mat4 lhs_normal;
	aabb_center(center, rhs);
	aabb_extent(extent, rhs);
	center[3] = 1;
	extent[3] = 1;

	mat4_vec4_multiply(center, lhs, center);

	mat4_set(lhs_normal, lhs);
	mat4_transpose(lhs_normal, lhs_normal);
	mat4_inverse(lhs_normal, lhs_normal);

	mat4_vec4_multiply(extent, lhs_normal, extent);

	vec3_sub(&dest[0], center, extent);
	vec3_add(&dest[3], center, extent);
	aabb_from_points(dest, &dest[0], &dest[3]);
}

#define VEC2(v) {v[0], v[1]}
#define VEC3(v) {v[0], v[1], v[2]}
#define VEC4(v) {v[0], v[1], v[2], v[3]}
#define QUAT(v) {v[0], v[1], v[2], v[3]}

#endif
