#include "ui.h"
#include <GL/glew.h>

static const vec4 quad_vertecies_topleft[] = {
	{ 0, 0, 0, 0 },
	{ 0, 1, 0, 1 },
	{ 1, 1, 1, 1 },

	{ 0, 0, 0, 0 },
	{ 1, 0, 1, 0 },
	{ 1, 1, 1, 1 },
};

static void
_create_quad(unsigned int *vao, unsigned int *vbo, const vec4 *data, size_t data_len) {
	glGenVertexArrays(1, vao);
	glBindVertexArray(*vao);

	glGenBuffers(1, vbo);
	glBindBuffer(GL_ARRAY_BUFFER, *vbo);

	glBufferData(GL_ARRAY_BUFFER, data_len * sizeof(vec4), data, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(vec4), (void*)0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(vec4), (void*)sizeof(vec2));

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void
ui_init(struct ui *ui) {
	_create_quad(&ui->quad_vao, &ui->quad_vbo, quad_vertecies_topleft, 6);
}

void
ui_render_quad(const struct ui *ui) {
	glBindVertexArray(ui->quad_vao);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);
}
