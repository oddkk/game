#include "timing.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <GLFW/glfw3.h>

void begin_timed_block(struct timed_block *block, const char *name) {
	if (block->is_running) {
		fprintf(stderr, "Tried starting the timed block '%s' while '%s' was running\n", name, block->name);
		return;
	}

	if (block->name != 0)
		free(block->name);

	block->name = strdup(name);
	block->end = 0;
	block->begin = glfwGetTime();
	block->is_running = true;

	fprintf(stderr, "[%.6fs] %s begin\n", block->begin, block->name);
}

void end_timed_block(struct timed_block *block) {
	if (!block->is_running) {
		fprintf(stderr, "Tried stopping a timed block while none was running\n");
		return;
	}

	block->end = glfwGetTime();
	block->is_running = false;

	fprintf(stderr, "[%.6fs] %s done (%.3fms)\n", block->end, block->name, (block->end - block->begin) * 1000.0);
	if (block->name != 0) {
		free(block->name);
		block->name = 0;
	}
}
