#include <stdio.h>
#include <stdbool.h>

#define ASSETS_BASE_DIR "assets"

char *assets_get_path(const char *asset, const char *owner);
