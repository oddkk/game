#include "object_render.h"
#include "ui.h"
#include "timing.h"
#include <GL/glew.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const vec3 box_vertecies[] = {
	// Top (Y+)
	{  1,  1,  1 }, {  0,  1,  0 },
	{  1,  1, -1 }, {  0,  1,  0 },
	{ -1,  1, -1 }, {  0,  1,  0 },

	{  1,  1,  1 }, {  0,  1,  0 },
	{ -1,  1, -1 }, {  0,  1,  0 },
	{ -1,  1,  1 }, {  0,  1,  0 },

	// Bottom (Y-)
	{ -1, -1, -1 }, {  0, -1,  0 },
	{  1, -1, -1 }, {  0, -1,  0 },
	{  1, -1,  1 }, {  0, -1,  0 },

	{ -1, -1,  1 }, {  0, -1,  0 },
	{ -1, -1, -1 }, {  0, -1,  0 },
	{  1, -1,  1 }, {  0, -1,  0 },

	// Front (Z+)
	{  1, -1, -1 }, {  0,  0,  1 },
	{ -1, -1, -1 }, {  0,  0,  1 },
	{ -1,  1, -1 }, {  0,  0,  1 },

	{  1,  1, -1 }, {  0,  0,  1 },
	{  1, -1, -1 }, {  0,  0,  1 },
	{ -1,  1, -1 }, {  0,  0,  1 },

	// Back (Z-)
	{ -1, -1,  1 }, {  0,  0, -1 },
	{  1, -1,  1 }, {  0,  0, -1 },
	{  1,  1,  1 }, {  0,  0, -1 },

	{ -1,  1,  1 }, {  0,  0, -1 },
	{ -1, -1,  1 }, {  0,  0, -1 },
	{  1,  1,  1 }, {  0,  0, -1 },

	// Right (X+)
	{  1, -1,  1 }, {  1,  0,  0 },
	{  1, -1, -1 }, {  1,  0,  0 },
	{  1,  1, -1 }, {  1,  0,  0 },

	{  1,  1,  1 }, {  1,  0,  0 },
	{  1, -1,  1 }, {  1,  0,  0 },
	{  1,  1, -1 }, {  1,  0,  0 },

	// Left (X-)
	{ -1, -1,  1 }, { -1,  0,  0 },
	{ -1,  1,  1 }, { -1,  0,  0 },
	{ -1,  1, -1 }, { -1,  0,  0 },

	{ -1, -1, -1 }, { -1,  0,  0 },
	{ -1, -1,  1 }, { -1,  0,  0 },
	{ -1,  1, -1 }, { -1,  0,  0 },
};

bool
object_render_init(struct object_render *data, shader *shader, const struct ui *ui) {
	data->ui = ui;
	data->shader = shader;

	BEGIN_TIMED_BLOCK(load_render_shader);

	data->uniform.modelview
		= glGetUniformLocation(data->shader->program, "in_modelview");
	data->uniform.modelview_normal
		= glGetUniformLocation(data->shader->program, "in_modelview_normal");
	data->uniform.projection
		= glGetUniformLocation(data->shader->program, "in_projection");
	data->uniform.camera_pos
		= glGetUniformLocation(data->shader->program, "in_camera_pos");
	data->uniform.noise
		= glGetUniformLocation(data->shader->program, "in_noise");
	data->uniform.dissolve
		= glGetUniformLocation(data->shader->program, "in_dissolve");
	data->uniform.transparancy
		= glGetUniformLocation(data->shader->program, "in_transparancy");

	END_TIMED_BLOCK(load_render_shader);

	data->model_shader_data = 
		model_make_model_shader(data->shader->program);

	glGenVertexArrays(1, &data->box_vao);
	glBindVertexArray(data->box_vao);

	glGenBuffers(1, &data->box_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, data->box_vbo);

	glBufferData(GL_ARRAY_BUFFER, sizeof(box_vertecies), box_vertecies, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vec3)*2, (void*)0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(vec3)*2, (void*)0);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(vec3)*2, (void*)sizeof(vec3));

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	return true;
}

void
object_render_free(struct object_render *data) {
	shader_free(data->shader);
}

void
object_render_resize(struct object_render *data, vec2i fbsize) {
	mat4_perspective(data->perspective, 90, (double)fbsize[0]/(double)fbsize[1],
					0.001, 100);
}

bool
object_render_queue_init(struct object_render_queue *q) {
	q->capacity = RENDER_QUERY_CAPACITY;
	q->length = 0;
	q->queue = calloc(q->capacity, sizeof(struct object_render_entry));
	q->overflow = false;

	if (q->queue == NULL) {
		fprintf(stderr, "Could not allocate render list\n");
		return false;
	}
	
	return true;
}

void
object_render_queue_free(struct object_render_queue *q) {
	free(q->queue);
}

void
object_render_queue_push(struct object_render_queue *q, const struct object_render_entry *entry) {
	if (q->length >= q->capacity) {
		q->overflow = true;
		return;
	}
	memcpy(&q->queue[q->length++], entry, sizeof(struct object_render_entry));
}

void
object_render_queue_clear(struct object_render_queue *q) {
	q->overflow = false;
	q->length = 0;
	vec3_set(q->camera_position, (vec3){0,0,0});
	vec3_set(q->camera_position, (vec3){0,0,0});
	quat_set(q->camera_rotation, (quat){0,0,0,0});
}

#include "window.h"
#include <time.h>

void
object_render_queue_render(struct object_render *data, const struct object_render_queue *q, struct window *w) {
	mat4 projection, modelview, debug_cube;

	glUseProgram(data->shader->program);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);

	mat4_quat_into(projection, data->perspective, q->camera_rotation);
	mat4_translate_into(projection, projection, (vec3){
		-q->camera_position[0],
		-q->camera_position[1],
		-q->camera_position[2]
	});
	glUniformMatrix4fv(data->uniform.projection, 1,
					   GL_TRUE, projection);

	glUniform3fv(data->uniform.camera_pos, 1, q->camera_position);
	glUniform1i(data->uniform.noise, 2);

	for (size_t i = 0; i < q->length; ++i) {
		struct object_render_entry *obj = &q->queue[i];

		mat4_set(modelview, obj->modelview);

		glUniformMatrix4fv(data->uniform.modelview, 1,
						   GL_TRUE, modelview);

		// Upload modelview normal
		mat4_transpose(modelview, modelview);
		mat4_inverse(modelview, modelview);
		glUniformMatrix4fv(data->uniform.modelview_normal, 1,
							GL_TRUE, modelview);

		glUniform1f(data->uniform.dissolve, obj->dissolve);
		glUniform1f(data->uniform.transparancy, 1.0);

		model_draw(obj->model, &data->model_shader_data);

		/*
		glBindVertexArray(data->box_vao);
		glUniform3fv(data->model_shader_data.in_ambient, 1,
		             (vec3){ 0.7, 0.7, 1.0 });
		glUniform3fv(data->model_shader_data.in_diffuse, 1,
		             (vec3){ 0, 0, 0 });
		glUniform3fv(data->model_shader_data.in_specular, 1,
		             (vec3){ 0, 0, 0 });
		glUniform1f(data->model_shader_data.in_specular_exp,
		            0);
		glUniform1f(data->uniform.transparancy,
		            0.5);

		aabb trans_bb;
		vec3 center, scale;

		mat4_aabb_multiply(trans_bb, obj->modelview, obj->model->bounding_box);
		aabb_center(center, trans_bb);
		aabb_extent(scale, trans_bb);

		mat4_translate(debug_cube, center);
		mat4_scale_into(debug_cube, debug_cube, scale);

		glUniformMatrix4fv(data->uniform.modelview, 1,
						   GL_TRUE, debug_cube);

		glDepthFunc(GL_LEQUAL);
		//glEnable(GL_BLEND);
		glDrawArrays(GL_TRIANGLES, 0, 36);
		glBindVertexArray(0);
		glDisable(GL_BLEND);
		glDepthFunc(GL_LESS);
		*/
	}
}
