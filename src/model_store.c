#include "model_store.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

model *
model_store_get(model_store *store, const char *path) {
	struct model_store_entry *last = NULL;
	struct model_store_entry *entry;

	for (struct model_store_entry *current = store->first_entry;
	     current != NULL; last = current, current = current->next) {
		if (strcmp(path, current->path) == 0) {
			if (!current->model.loaded) {
				model_load_from_file(&current->model, current->path);
			}

			++current->num_users;
			return &current->model;
		}
	}

	entry = calloc(1, sizeof(struct model_store_entry));

	if (entry == NULL) {
		fprintf(stderr, "Could not allocate memory for model store entry");
		abort();
	}

	entry->path = strdup(path);

	if (last != NULL) {
		assert(last->next == NULL);

		last->next = entry;
	} else {
		assert(store->first_entry == NULL);

		store->first_entry = entry;
	}
	
	model_load_from_file(&entry->model, path);

	entry->num_users = 1;

	return &entry->model;
}

void
model_store_release(model_store *store, model *model) {
	struct model_store_entry *entry = NULL;

	for (struct model_store_entry *current = store->first_entry;
	     current != NULL; current = current->next) {
		if (&current->model == model) {
			entry = current;
		}
	}

	if (entry == NULL)
		return;

	if (--entry->num_users <= 0) {
		model_unload(&entry->model);
	}
}

void
model_store_free(model_store *store) {
	struct model_store_entry *current = store->first_entry;
	while (current != NULL) {
		struct model_store_entry *next = current->next;

		model_unload(&current->model);

		free(current->path);
		free(current);
		current = next;
	}
	store->first_entry = NULL;
}
