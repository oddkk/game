#include "ui.h"
#include "linmath.h"
#include "shader.h"

struct framebuffer_info {
};

struct framebuffer {
	unsigned int fbo, fbo_tex, fbo_depth, fbo_plane_buffer, fbo_plane_vao;
	shader *shader;
	struct  {
		int framebuffer;
		int screen_size;
	} uniform;

	vec2i target_size;
	struct ui *ui;
};

void framebuffer_init(struct framebuffer *fb, struct ui *ui);
void framebuffer_bind_shader(struct framebuffer *fb, shader *shdr);
void framebuffer_resize(struct framebuffer *fb, vec2i size);
void framebuffer_begin_render(const struct framebuffer *fb);
void framebuffer_end_render(const struct framebuffer *fb);
void framebuffer_render(const struct framebuffer *fb);
void framebuffer_free(struct framebuffer *fb);
