#ifndef GAME_ARGUMENTS_H
#define GAME_ARGUMENTS_H

#include "window.h"

struct arguments {
	window_mode win_mode;
	int width, height;
};

void arguments_parse(struct arguments *args, int argc, char *argv[]);

#endif
