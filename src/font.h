#include <stddef.h>
#include <stdbool.h>
#include "linmath.h"
#include "shader.h"
#include "ui.h"

#define FONT_ATLAS_WIDTH 128
#define FONT_ATLAS_CAPACITY_INCREMENT 64

struct font_context {
	void *library_handle;
	shader font_shader;
	unsigned int uniform_screen_size;
	unsigned int uniform_atlas;
	unsigned int uniform_atlas_size;
	unsigned int uniform_glyph_pos;
	unsigned int uniform_glyph_size;
	unsigned int uniform_glyph_bearing;
	unsigned int uniform_cursor;
	unsigned int uniform_matrix;
	unsigned int uniform_color;
	struct ui *ui;
};

struct font_glyph {
	unsigned int character;
	vec2 bearing;
	vec2 advance;
	vec2i atlas_position;
	vec2i size;
};

struct font_atlas_line {
	vec2i pos;
	int width;
};

struct font_size {
	// Note that the size is rounded to nearest pt/64
	float size;

	float line_height;

	unsigned int atlas_texture;
	unsigned char *atlas;
	vec2i atlas_size;

	//struct font_atlas_line surface[FONT_ATLAS_MAX_SURFACE_LINES+1];
	size_t surface[FONT_ATLAS_WIDTH];

	struct font_glyph *glyphs;
	size_t glyphs_num;
	size_t glyphs_capacity;

	// Provide the standard latin characters in O(1) time
	struct {
		bool initialized;
		struct font_glyph glyph;
	} default_glyphs['~'-' '+1];
};

struct font {
	struct font_context *context;
	void *face_handle;

	struct font_size *sizes;
	size_t sizes_num;
};

bool font_context_init(struct font_context *lib);
void font_context_free(struct font_context *lib);

bool font_from_file(struct font_context *lib, struct font *face, const char *path);
bool font_from_asset(struct font_context *lib, struct font *face, const char *asset, const char *owner);

struct font_size *font_get_size(struct font *face, float size);

struct font_glyph *font_get_glyph(struct font *face, unsigned int character, float size);
struct font_glyph *font_size_get_glyph(struct font *face, struct font_size *size, unsigned int character);

void font_printf(struct font *face, float size, vec2 position, char *format, ...);
void font_printf_mat(struct font *face, float size, mat3 matrix, char *format, ...);

void font_size_free(struct font_size *size);
void font_free(struct font *face);
