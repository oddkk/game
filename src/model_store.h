#ifndef GAME_MODELSTORE_H
#define GAME_MODELSTORE_H

#include "model.h"

struct model_store_entry {
	char *path;
	model model;
	int num_users;
	struct model_store_entry *next;
};

typedef struct {
	struct model_store_entry *first_entry;
} model_store;

model *model_store_get(model_store *store, const char *path);
void model_store_release(model_store *store, model *model);
void model_store_free(model_store *store);

#endif
