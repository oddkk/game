#ifndef GAME_SHADER_H
#define GAME_SHADER_H

#include <stdbool.h>

#define NUM_SHADER_TYPES (7)

typedef enum {
	SHADER_NONE = 0,
	SHADER_COMPUTE,
	SHADER_TESS_CONTROL,
	SHADER_TESS_EVAL,
	SHADER_GEOMETRY,
	SHADER_VERTEX,
	SHADER_FRAGMENT,
} shader_type;

typedef struct {
	unsigned int shader_id[NUM_SHADER_TYPES];
	unsigned int program;
} shader;

typedef struct {
	shader_type type;
	char *data;
} shader_part;

/*
  A helper to create a shader program

  parts: A list of shader parts to be added to the new shader
  program. This list should be terminated by an element with type 0.
 */
bool shader_create(shader *shader, shader_part *parts);
bool shader_init(shader *shader);
bool shader_add(shader *shader, shader_type type, const char *data);
bool shader_finalize(shader *shader);
void shader_free(shader *shader);

bool shader_from_file(shader *shader, const char *path);

#endif
