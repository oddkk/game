#include "shader.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <GL/glew.h>
#include "array.h"
#include "assets.h"

static struct {
	GLenum type;
	const char *name;
} shader_types[] = {
	[SHADER_COMPUTE]      = { GL_COMPUTE_SHADER,         "COMPUTE" },
	[SHADER_TESS_CONTROL] = { GL_TESS_CONTROL_SHADER,    "TESS_CONTROL" },
	[SHADER_TESS_EVAL]    = { GL_TESS_EVALUATION_SHADER, "TESS_EVALUATION" },
	[SHADER_GEOMETRY]     = { GL_GEOMETRY_SHADER,        "GEOMETRY" },
	[SHADER_VERTEX]       = { GL_VERTEX_SHADER,          "VERTEX"   },
	[SHADER_FRAGMENT]     = { GL_FRAGMENT_SHADER,        "FRAGMENT" },
};

bool shader_create(shader *shader, shader_part *parts) {
	if (!shader_init(shader)) {
		return false;
	}

	for (shader_part *part = parts; part->type != 0; ++part) {
		if (!shader_add(shader, part->type, part->data)) {
			return false;
		}
	}

	if (!shader_finalize(shader)) {
		return false;
	}
	
	return true;
}

bool shader_init(shader *shader) {
	GLenum err;

	for (int i = 0; i < NUM_SHADER_TYPES; ++i) {
		shader->shader_id[i] = 0;
	}

	shader->program = glCreateProgram();
	if ((err = glGetError()) != GL_NO_ERROR) {
		fprintf(stderr, "Could create shader program (%i)", err);
		return false;
	}

	return true;
}

bool shader_add(shader *shader, shader_type type, const char *data) {
	GLenum err;
	GLuint id;

	if (type > NUM_SHADER_TYPES || type < 1) {
		fprintf(stderr, "Invalid shader type %i\n", type);
		return false;
	}

	if (shader->shader_id[type] != 0) {
		glDetachShader(shader->program, shader->shader_id[type]);
		glDeleteShader(shader->shader_id[type]);
		shader->shader_id[type] = 0;
	}

	id = glCreateShader(shader_types[type].type);
	shader->shader_id[type] = id;
	if ((err = glGetError()) != GL_NO_ERROR) {
		fprintf(stderr, "Could not create %s shader (%i)", shader_types[type].name, err);
		return false;
	}

	glShaderSource(id, 1, &data, NULL);
	if ((err = glGetError()) != GL_NO_ERROR) {
		fprintf(stderr, "Could upload %s shader (%i)", shader_types[type].name, err);
		return false;
	}

	glCompileShader(id);
	if ((err = glGetError()) != GL_NO_ERROR) {
		fprintf(stderr, "Could compile %s shader (%i)", shader_types[type].name, err);
		return false;
	}

	GLint compile_status;

	glGetObjectParameterivARB(id, GL_OBJECT_COMPILE_STATUS_ARB, &compile_status);

	if (compile_status == GL_FALSE) {
		GLint len;
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &len);

		if (len > 0) {
			GLchar *str = calloc(len, sizeof(GLchar));
			glGetShaderInfoLog(id, len, NULL, str);
			fprintf(stderr, "Failed to compile %s shader: %s\n", shader_types[type].name, str);
			free(str);
		}
		else {
			fprintf(stderr, "Failed to compile %s shader\n", shader_types[type].name);
		}

		return false;
	}

	glAttachShader(shader->program, id);
	if ((err = glGetError()) != GL_NO_ERROR) {
		fprintf(stderr, "Could attach %s shader (%i)", shader_types[type].name, err);
		return false;
	}
	
	return true;
}

bool shader_finalize(shader *shader) {
	GLenum err;

	glLinkProgram(shader->program);
	if ((err = glGetError()) != GL_NO_ERROR) {
		fprintf(stderr, "Could link program (%i)", err);
		return false;
	}

	GLint link_status;

	glGetObjectParameterivARB(shader->program, GL_OBJECT_LINK_STATUS_ARB, &link_status);

	if (link_status == GL_FALSE) {
		GLint len;
		glGetProgramiv(shader->program, GL_INFO_LOG_LENGTH, &len);

		if (len > 0) {
			GLchar *str = calloc(len, sizeof(GLchar));
			glGetProgramInfoLog(shader->program, len, NULL, str);
			fprintf(stderr, "Failed to link program: %s\n", str);
			free(str);
		}
		else {
			fprintf(stderr, "Failed to link program\n");
		}

		return false;
	}

	return true;
}

void shader_free(shader *shader) {
	glDeleteProgram(shader->program);
	for (int i = 0; i < NUM_SHADER_TYPES; ++i) {
		if (shader->shader_id[i] != 0) {
			glDeleteShader(shader->shader_id[i]);
			shader->shader_id[i] = 0;
		}
	}
}

DARRAY(shader_part);

static char *load_text_file(FILE *fp) {
	char *data = NULL;
	size_t length;

	if (fp == NULL) {
		return NULL;
	}

	fseek(fp, 0, SEEK_END);
	length = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	data = malloc(length + 1);

	if (data) {
		data[length] = '\0';
		fread(data, 1, length, fp);
	}

	return data;
}

static void load_shader_part(array_shader_part *array, const char *asset, const char *owner, shader_type type) {
	char *path = assets_get_path(asset, owner);
	FILE *fp = NULL;

	if (path != NULL) {
		fp = fopen(path, "r");
	}

	shader_part part = {
		.type = type,
		.data = load_text_file(fp),
	};
	
	free(path);
	if (!part.data) {
		fprintf(stderr, "Could not load shader %s\n", asset);
		return;
	}
	array_shader_part_append(array, &part);
}

bool shader_from_file(shader *shader, const char *asset) {
	const char *token_separator = " \n\r";
	array_shader_part parts = {};
	size_t line_len = 0;
	char *line = NULL;
	char *path = NULL;
	FILE *fp = NULL;

	path = assets_get_path(asset, NULL);

	fp = fopen(path, "r");

	if (!fp) {
		free(path);
		return false;
	}

	while (getline(&line, &line_len, fp) != -1) {
		char *line_state;
		char *token = strtok_r(line, token_separator, &line_state);

		if (!token)
			continue;

		/*  */ if (strcmp(token, "Sv") == 0) {
			load_shader_part(&parts, strtok_r(NULL, token_separator, &line_state), path,
							 SHADER_VERTEX);
		} else if (strcmp(token, "Sf") == 0) {
			load_shader_part(&parts, strtok_r(NULL, token_separator, &line_state), path,
							 SHADER_FRAGMENT);
		} else if (strcmp(token, "Sg") == 0) {
			load_shader_part(&parts, strtok_r(NULL, token_separator, &line_state), path,
							 SHADER_GEOMETRY);
		} else if (strcmp(token, "Stc") == 0) {
			load_shader_part(&parts, strtok_r(NULL, token_separator, &line_state), path,
							 SHADER_TESS_CONTROL);
		} else if (strcmp(token, "Ste") == 0) {
			load_shader_part(&parts, strtok_r(NULL, token_separator, &line_state), path,
							 SHADER_TESS_EVAL);
		} else if (strcmp(token, "Sc") == 0) {
			load_shader_part(&parts, strtok_r(NULL, token_separator, &line_state), path,
							 SHADER_COMPUTE);
		}
	}

	free(path);
	free(line);
	fclose(fp);

	{
		shader_part empty_part = {
			.type = SHADER_NONE,
			.data = NULL,
		};
		array_shader_part_append(&parts, &empty_part);
	}

	bool res = shader_create(shader, parts.data);

	for (size_t i = 0; i < parts.size; ++i) {
		free(parts.data[i].data);
	}

	free(parts.data);

	return res;
}
