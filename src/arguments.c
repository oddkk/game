#include "arguments.h"
#include <argp.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

static char argp_doc[] = "Game";
static char doc[] = "Game";

static struct argp_option options[] = {
	{ "fullscreen", 'f', 0,                  0, "Start game in fullscreen mode", 0 },
	{ "windowed",   'w', 0,                  0, "Start game in windowed mode",   0 },
	{ "resolution", 'r', "<width>x<height>", 0, "The resolution of the game",    0 },
	{}
};

static error_t
parse_opt(int key, char *arg, struct argp_state *state) {
	(void)arg;
	struct arguments *args = state->input;
	switch (key) {
	case 'f':
		args->win_mode |= WINDOW_MODE_FULLSCREEN;
		break;
	case 'w':
		args->win_mode |= WINDOW_MODE_WINDOWED;
		break;
	case 'r': {
		char *separator = strchr(arg, 'x');
		if (!separator) {
			argp_error(state, "Resolution should have format <width>x<height>");
			return EINVAL;
		}
		args->width  = strtol(arg, &separator, 10);
		args->height = strtol(++separator, 0, 10);

	} break;
	default:
		return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

static struct argp argp = { options, parse_opt, argp_doc, doc, 0, 0, 0 };

void
arguments_parse(struct arguments *args, int argc, char *argv[]) {
	argp_parse(&argp, argc, argv, 0, 0, args);
}
